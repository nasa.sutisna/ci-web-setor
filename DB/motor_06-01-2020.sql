-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2020 at 11:02 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `minor`
--

-- --------------------------------------------------------

--
-- Table structure for table `motor`
--

CREATE TABLE `motor` (
  `motorId` varchar(10) NOT NULL COMMENT 'Plat Nomor',
  `vendorId` varchar(15) NOT NULL,
  `city` varchar(50) NOT NULL,
  `colour` varchar(15) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'ready' COMMENT 'ready or rented'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `motor`
--

INSERT INTO `motor` (`motorId`, `vendorId`, `city`, `colour`, `status`) VALUES
('B 1001 JKT', '20200106090341', 'Jakarta', 'Hitam', 'ready'),
('B 1002 JKT', '20200106090341', 'Jakarta', 'Merah', 'ready'),
('B 1003 JKT', '20200106090341', 'Jakarta', 'Biru', 'ready'),
('B 1004 JKT', '20200106090341', 'Jakarta', 'Hitam', 'ready'),
('B 1005 JKT', '20200106090511', 'Jakarta', 'Merah', 'ready'),
('B 1006 JKT', '20200106090511', 'Jakarta', 'Biru', 'ready'),
('B 1007 JKT', '20200106090647', 'Jakarta', 'Hitam', 'ready'),
('B 1008 JKT', '20200106090647', 'Jakarta', 'Silver', 'ready'),
('B 1009 JKT', '20200106090736', 'Jakarta', 'Biru', 'ready'),
('B 1010 JKT', '20200106090736', 'Jakarta', 'Merah', 'ready'),
('B 1011 JKT', '20200106090736', 'Jakarta', 'Putih', 'ready'),
('B 1012 JKT', '20200106090822', 'Jakarta', 'Hijau', 'ready'),
('B 1013 JKT', '20200106090822', 'Jakarta', 'Merah', 'ready'),
('B 1014 JKT', '20200106091035', 'Jakarta', 'Silver', 'ready'),
('B 1015 JKT', '20200106091035', 'Jakarta', 'Merah', 'ready'),
('B 1016 JKT', '20200106091144', 'Jakarta', 'Merah', 'ready'),
('B 1017 JKT', '20200106091144', 'Jakarta', 'Hitam', 'ready'),
('B 1018 JKT', '20200106091144', 'Jakarta', 'Putih', 'ready'),
('B 1019 JKT', '20200106091324', 'Jakarta', 'Merah', 'ready'),
('B 1020 JKT', '20200106091324', 'Jakarta', 'Biru', 'ready'),
('B 1021 JKT', '20200106091649', 'Jakarta', 'Merah', 'ready'),
('B 1022 JKT', '20200106091649', 'Jakarta', 'Hitam', 'ready'),
('B 1023 JKT', '20200106091649', 'Jakarta', 'Silver', 'ready'),
('B 1024 JKT', '20200106091803', 'Jakarta', 'Merah', 'ready'),
('T 1005 BDG', '20200106090341', 'Bandung', 'Merah', 'ready'),
('T 1006 BDG', '20200106090341', 'Bandung', 'Putih', 'ready'),
('T 1007 BDG', '20200106090341', 'Bandung', 'Biru', 'ready'),
('T 1008 BDG', '20200106090511', 'Bandung', 'Putih', 'ready'),
('T 1009 BDG', '20200106090511', 'Bandung', 'Putih', 'ready'),
('T 1010 BDG', '20200106090511', 'Bandung', 'Putih', 'ready'),
('T 1011 BDG', '20200106090511', 'Bandung', 'Putih', 'ready'),
('T 1012 BDG', '20200106090511', 'Bandung', 'Hitam', 'ready'),
('T 1013 BDG', '20200106090511', 'Bandung', 'Hitam', 'ready'),
('T 1014 BDG', '20200106090511', 'Bandung', 'Hitam', 'ready'),
('T 1015 BDG', '20200106090511', 'Bandung', 'Biru', 'ready'),
('T 1016 BDG', '20200106090647', 'Bandung', 'Merah', 'ready'),
('T 1017 BDG', '20200106090647', 'Bandung', 'Hitam', 'ready'),
('T 1018 BDG', '20200106090647', 'Bandung', 'Silver', 'ready'),
('T 1019 BDG', '20200106090736', 'Bandung', 'Hitam', 'ready'),
('T 1020 BDG', '20200106090736', 'Bandung', 'Merah', 'ready'),
('T 1021 BDG', '20200106090736', 'Bandung', 'Biru', 'ready'),
('T 1022 BDG', '20200106090822', 'Bandung', 'Hitam', 'ready'),
('T 1023 BDG', '20200106090822', 'Bandung', 'Merah', 'ready'),
('T 1024 BDG', '20200106091035', 'Bandung', 'Merah', 'ready'),
('T 1025 BDG', '20200106091035', 'Bandung', 'Hitam', 'ready'),
('T 1026 BDG', '20200106091035', 'Bandung', 'Silver', 'ready'),
('T 1027 BDG', '20200106091144', 'Bandung', 'Putih', 'ready'),
('T 1028 BDG', '20200106091144', 'Bandung', 'Merah', 'ready'),
('T 1029 BDG', '20200106091144', 'Bandung', 'Hitam', 'ready'),
('T 1030 BDG', '20200106091144', 'Bandung', 'Biru', 'ready'),
('T 1031 BDG', '20200106091324', 'Bandung', 'Biru', 'ready'),
('T 1032 BDG', '20200106091324', 'Bandung', 'Hitam', 'ready'),
('T 1033 BDG', '20200106091324', 'Bandung', 'Hitam', 'ready'),
('T 1034 BDG', '20200106091324', 'Bandung', 'Merah', 'ready'),
('T 1035 BDG', '20200106091649', 'Bandung', 'Silver', 'ready'),
('T 1036 BDG', '20200106091649', 'Bandung', 'Merah', 'ready'),
('T 1037 BDG', '20200106091803', 'Bandung', 'Merah', 'ready'),
('T 1038 BDG', '20200106091803', 'Bandung', 'Merah', 'ready'),
('T 1039 BDG', '20200106091803', 'Bandung', 'Putih', 'ready');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `motor`
--
ALTER TABLE `motor`
  ADD PRIMARY KEY (`motorId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
