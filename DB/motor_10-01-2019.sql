-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 10, 2020 at 05:38 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_minor`
--

-- --------------------------------------------------------

--
-- Table structure for table `motor`
--

CREATE TABLE `motor` (
  `motorId` varchar(10) NOT NULL COMMENT 'Plat Nomor',
  `vendorId` varchar(15) NOT NULL,
  `city` varchar(50) NOT NULL,
  `colour` varchar(15) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'ready' COMMENT 'ready or rented'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `motor`
--

INSERT INTO `motor` (`motorId`, `vendorId`, `city`, `colour`, `status`) VALUES
('B1001JKT', '20200106090341', 'Jakarta', 'Hitam', 'ready'),
('B1002JKT', '20200106090341', 'Jakarta', 'Merah', 'ready'),
('B1003JKT', '20200106090341', 'Jakarta', 'Biru', 'ready'),
('B1004JKT', '20200106090341', 'Jakarta', 'Hitam', 'ready'),
('B1005JKT', '20200106090511', 'Jakarta', 'Merah', 'ready'),
('B1006JKT', '20200106090511', 'Jakarta', 'Biru', 'ready'),
('B1007JKT', '20200106090647', 'Jakarta', 'Hitam', 'ready'),
('B1008JKT', '20200106090647', 'Jakarta', 'Silver', 'ready'),
('B1009JKT', '20200106090736', 'Jakarta', 'Biru', 'ready'),
('B1010JKT', '20200106090736', 'Jakarta', 'Merah', 'ready'),
('B1011JKT', '20200106090736', 'Jakarta', 'Putih', 'ready'),
('B1012JKT', '20200106090822', 'Jakarta', 'Hijau', 'ready'),
('B1013JKT', '20200106090822', 'Jakarta', 'Merah', 'ready'),
('B1014JKT', '20200106091035', 'Jakarta', 'Silver', 'ready'),
('B1015JKT', '20200106091035', 'Jakarta', 'Merah', 'ready'),
('B1016JKT', '20200106091144', 'Jakarta', 'Merah', 'ready'),
('B1017JKT', '20200106091144', 'Jakarta', 'Hitam', 'ready'),
('B1018JKT', '20200106091144', 'Jakarta', 'Putih', 'ready'),
('B1019JKT', '20200106091324', 'Jakarta', 'Merah', 'ready'),
('B1020JKT', '20200106091324', 'Jakarta', 'Biru', 'ready'),
('B1021JKT', '20200106091649', 'Jakarta', 'Merah', 'ready'),
('B1022JKT', '20200106091649', 'Jakarta', 'Hitam', 'ready'),
('B1023JKT', '20200106091649', 'Jakarta', 'Silver', 'ready'),
('B1024JKT', '20200106091803', 'Jakarta', 'Merah', 'ready'),
('D1005BDG', '20200106090341', 'Bandung', 'Merah', 'ready'),
('D1006BDG', '20200106090341', 'Bandung', 'Putih', 'ready'),
('D1007BDG', '20200106090341', 'Bandung', 'Biru', 'ready'),
('D1008BDG', '20200106090511', 'Bandung', 'Putih', 'ready'),
('D1009BDG', '20200106090511', 'Bandung', 'Putih', 'ready'),
('D1010BDG', '20200106090511', 'Bandung', 'Putih', 'ready'),
('D1011BDG', '20200106090511', 'Bandung', 'Putih', 'ready'),
('D1012BDG', '20200106090511', 'Bandung', 'Hitam', 'ready'),
('D1013BDG', '20200106090511', 'Bandung', 'Hitam', 'ready'),
('D1014BDG', '20200106090511', 'Bandung', 'Hitam', 'ready'),
('D1015BDG', '20200106090511', 'Bandung', 'Biru', 'ready'),
('D1016BDG', '20200106090647', 'Bandung', 'Merah', 'ready'),
('D1017BDG', '20200106090647', 'Bandung', 'Hitam', 'ready'),
('D1018BDG', '20200106090647', 'Bandung', 'Silver', 'ready'),
('D1019BDG', '20200106090736', 'Bandung', 'Hitam', 'ready'),
('D1020BDG', '20200106090736', 'Bandung', 'Merah', 'ready'),
('D1021BDG', '20200106090736', 'Bandung', 'Biru', 'ready'),
('D1022BDG', '20200106090822', 'Bandung', 'Hitam', 'ready'),
('D1023BDG', '20200106090822', 'Bandung', 'Merah', 'ready'),
('D1024BDG', '20200106091035', 'Bandung', 'Merah', 'ready'),
('D1025BDG', '20200106091035', 'Bandung', 'Hitam', 'ready'),
('D1026BDG', '20200106091035', 'Bandung', 'Silver', 'ready'),
('D1027BDG', '20200106091144', 'Bandung', 'Putih', 'ready'),
('D1028BDG', '20200106091144', 'Bandung', 'Merah', 'ready'),
('D1029BDG', '20200106091144', 'Bandung', 'Hitam', 'ready'),
('D1030BDG', '20200106091144', 'Bandung', 'Biru', 'ready'),
('D1031BDG', '20200106091324', 'Bandung', 'Biru', 'ready'),
('D1032BDG', '20200106091324', 'Bandung', 'Hitam', 'ready'),
('D1033BDG', '20200106091324', 'Bandung', 'Hitam', 'ready'),
('D1034BDG', '20200106091324', 'Bandung', 'Merah', 'ready'),
('D1035BDG', '20200106091649', 'Bandung', 'Silver', 'ready'),
('D1036BDG', '20200106091649', 'Bandung', 'Merah', 'ready'),
('D1037BDG', '20200106091803', 'Bandung', 'Merah', 'ready'),
('D1038BDG', '20200106091803', 'Bandung', 'Merah', 'ready'),
('D1039BDG', '20200106091803', 'Bandung', 'Putih', 'ready');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `motor`
--
ALTER TABLE `motor`
  ADD PRIMARY KEY (`motorId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
