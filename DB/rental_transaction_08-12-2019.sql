-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2019 at 11:28 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `minor`
--

-- --------------------------------------------------------

--
-- Table structure for table `rental_transaction`
--

CREATE TABLE `rental_transaction` (
  `transactionId` varchar(20) NOT NULL,
  `memberId` varchar(15) NOT NULL,
  `vendorId` varchar(15) NOT NULL,
  `motorId` varchar(10) DEFAULT NULL,
  `rentalDate` datetime NOT NULL,
  `returnDate` datetime NOT NULL,
  `duration` int(11) NOT NULL COMMENT 'day',
  `startPickupLocation` varchar(100) NOT NULL,
  `endPickupLocation` varchar(100) NOT NULL,
  `totalOverDue` int(11) NOT NULL DEFAULT '0' COMMENT 'hour',
  `overDueFee` int(11) NOT NULL DEFAULT '0',
  `totalAmount` int(11) NOT NULL,
  `proofOfPayment` varchar(100) DEFAULT NULL,
  `status` varchar(10) NOT NULL COMMENT 'order, payment, rent, return'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_transaction`
--

INSERT INTO `rental_transaction` (`transactionId`, `memberId`, `vendorId`, `motorId`, `rentalDate`, `returnDate`, `duration`, `startPickupLocation`, `endPickupLocation`, `totalOverDue`, `overDueFee`, `totalAmount`, `proofOfPayment`, `status`) VALUES
('mnr-20191208112321', '5', '20191203033348', NULL, '2019-12-08 19:00:00', '2019-12-09 19:00:00', 1, 'Stasiun Bandung', 'Airy Echo Dipatiukur', 0, 0, 150000, NULL, 'ORDER');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rental_transaction`
--
ALTER TABLE `rental_transaction`
  ADD PRIMARY KEY (`transactionId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
