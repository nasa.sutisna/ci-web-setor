-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2020 at 11:02 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `minor`
--

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `vendorId` varchar(15) NOT NULL,
  `merk` varchar(20) NOT NULL,
  `type` varchar(30) NOT NULL,
  `price` int(11) NOT NULL,
  `overDueFee` int(11) NOT NULL DEFAULT '0',
  `stock` int(11) NOT NULL DEFAULT '0',
  `picture1` varchar(100) DEFAULT NULL,
  `picture2` varchar(100) DEFAULT NULL,
  `picture3` varchar(100) DEFAULT NULL,
  `picture4` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`vendorId`, `merk`, `type`, `price`, `overDueFee`, `stock`, `picture1`, `picture2`, `picture3`, `picture4`) VALUES
('20200106090341', 'Yamaha V-xion', 'Manual', 150000, 20000, 7, '1.jpg', '2.jpg', '3.png', '4.jpg'),
('20200106090511', 'Yamaha N-Max', 'Matic', 150000, 20000, 10, '1.jpg', '2.jpg', '3.jpg', '4.jpg'),
('20200106090647', 'Yamaha Soul GT', 'Matic', 100000, 10000, 5, '1.jpg', '2.jpg', '3.jpg', '4.jpg'),
('20200106090736', 'Yamaha Fino', 'Matic', 100000, 10000, 6, '1.jpg', '2.jpg', '3.jpg', '4.jpg'),
('20200106090822', 'Yamaha X-Ride', 'Matic', 100000, 10000, 4, '1.png', '4.jpg', '2.jpg', '3.jpg'),
('20200106091035', 'Honda PCX', 'Matic', 150000, 20000, 5, '1.jpg', '2.jpg', '3.jpg', '4.jpg'),
('20200106091144', 'Honda CBR', 'Manual', 150000, 20000, 7, '1.jpg', '2.jpg', '3.jpg', '4.jpg'),
('20200106091324', 'Honda Beat Street', 'Matic', 100000, 10000, 6, '1.jpg', '2.jpeg', '3.jpg', '4.jpg'),
('20200106091649', 'Honda Supra X', 'Manual', 120000, 10000, 5, '1.jpg', '2.jpg', '4.png', '3.jpg'),
('20200106091803', 'Honda Vario', 'Matic', 90000, 10000, 4, '1.jpg', '2.jpg', '4.jpg', '3.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`vendorId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
