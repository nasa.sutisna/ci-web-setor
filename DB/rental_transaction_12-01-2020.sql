-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2020 at 06:51 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `minor`
--

-- --------------------------------------------------------

--
-- Table structure for table `rental_transaction`
--

CREATE TABLE `rental_transaction` (
  `transactionId` varchar(20) NOT NULL,
  `memberId` varchar(15) NOT NULL,
  `vendorId` varchar(15) NOT NULL,
  `motorId` varchar(100) DEFAULT NULL,
  `rentalDate` varchar(12) NOT NULL,
  `returnDate` varchar(12) NOT NULL,
  `duration` int(11) NOT NULL COMMENT 'day',
  `startPickupLocation` varchar(100) NOT NULL,
  `endPickupLocation` varchar(100) NOT NULL,
  `itemTotal` int(11) NOT NULL,
  `totalOverDue` int(11) NOT NULL DEFAULT '0' COMMENT 'hour',
  `overDueFee` int(11) NOT NULL DEFAULT '0',
  `totalAmount` int(11) NOT NULL,
  `proofOfPayment` varchar(100) DEFAULT NULL,
  `status` varchar(10) NOT NULL COMMENT 'order, payment, rent, return',
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_transaction`
--

INSERT INTO `rental_transaction` (`transactionId`, `memberId`, `vendorId`, `motorId`, `rentalDate`, `returnDate`, `duration`, `startPickupLocation`, `endPickupLocation`, `itemTotal`, `totalOverDue`, `overDueFee`, `totalAmount`, `proofOfPayment`, `status`, `notes`) VALUES
('mnr-20200111064827', '1234562202789', '20200106091144', NULL, '13/01/2020', '15/01/2020', 2, 'Stasiun Bandung', 'Terminal leuwi panjang', 1, 0, 0, 300000, NULL, 'ORDER', 'klkl');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rental_transaction`
--
ALTER TABLE `rental_transaction`
  ADD PRIMARY KEY (`transactionId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
