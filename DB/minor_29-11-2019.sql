-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 29, 2019 at 08:32 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `minor`
--

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `memberId` int(11) NOT NULL,
  `fullName` varchar(100) NOT NULL,
  `gender` varchar(8) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `phoneNumber` varchar(12) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `ktp` varchar(100) NOT NULL,
  `sim` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`memberId`, `fullName`, `gender`, `address`, `email`, `phoneNumber`, `photo`, `ktp`, `sim`) VALUES
(5, 'feri', 'pria', 'Jl. sadewa 10 No. 137', 'ferimirpan@gmail.com', '085693766474', './images/ferimirpan@gmail.com/2.PNG', './images/ferimirpan@gmail.com/logo1.jpg', './images/ferimirpan@gmail.com/ktp.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `motor`
--

CREATE TABLE `motor` (
  `motorId` varchar(10) NOT NULL COMMENT 'Plat Nomor',
  `vendorId` varchar(10) NOT NULL,
  `city` varchar(50) NOT NULL,
  `colour` varchar(15) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'ready' COMMENT 'ready or rented'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `motor`
--

INSERT INTO `motor` (`motorId`, `vendorId`, `city`, `colour`, `status`) VALUES
('T0001A', 'a0002', 'jakarta', 'biru', 'ready'),
('T0001B', 'a0001', 'bandung', 'merah', 'ready');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `paymentId` int(11) NOT NULL,
  `transactionId` varchar(20) NOT NULL,
  `transferEvidence` varchar(20) NOT NULL COMMENT 'bukti transfer'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_transaction`
--

CREATE TABLE `rental_transaction` (
  `transactionId` varchar(20) NOT NULL,
  `memberId` varchar(15) NOT NULL,
  `motorId` varchar(10) NOT NULL,
  `rentalDate` datetime NOT NULL,
  `returnDate` datetime NOT NULL,
  `duration` int(11) NOT NULL COMMENT 'day',
  `startPickupLocation` varchar(100) NOT NULL,
  `endPickupLocation` varchar(100) NOT NULL,
  `totalOverDue` int(11) NOT NULL DEFAULT '0' COMMENT 'hour',
  `overDueFee` int(11) NOT NULL DEFAULT '0',
  `totalAmount` int(11) NOT NULL,
  `status` varchar(10) NOT NULL COMMENT 'order, rent, return'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `email` varchar(100) NOT NULL,
  `password` varchar(70) NOT NULL,
  `role` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`email`, `password`, `role`) VALUES
('ferimirpan@gmail.com', '343623c5e8ec4075fc8dc8c7f4bef03c83fa570dbce5862f237ff14b2d462cb5', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `vendorId` varchar(10) NOT NULL,
  `merk` varchar(20) NOT NULL,
  `type` varchar(30) NOT NULL,
  `overDueFee` int(11) NOT NULL DEFAULT '0',
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`memberId`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `motor`
--
ALTER TABLE `motor`
  ADD PRIMARY KEY (`motorId`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`paymentId`);

--
-- Indexes for table `rental_transaction`
--
ALTER TABLE `rental_transaction`
  ADD PRIMARY KEY (`transactionId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`email`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`vendorId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `memberId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
