<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Template
{
    public $template_data = array();

    public function set($name, $value)
    {
        $this->template_data[$name] = $value;
    }

    public function view($view = '', $view_data = array(), $return = false)
    {
        $this->CI = &get_instance();
        $this->set('contents', $this->CI->load->view($view, $view_data, true));
        return $this->CI->load->view('layout', $this->template_data, $return);
    }
}
