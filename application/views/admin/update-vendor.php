<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/custom/input.css') ?>">

    <!-- GLOBAL CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/global/global.css') ?>">


    <title>Input Vendor</title>
</head>

<body>

    <div class="title">
        <minor-title>Input Vendor</minor-title>
    </div>
    <?php echo isset($message) ? '<script>alert("' . $message . '")</script>' : ''; ?>
    <div class="body">
        <div class="container">

            <form action="<?php echo base_url() . 'vendor/updateVendor'; ?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                <input type="hidden" value="<?php echo $items->vendorId ?>" name="vendorId">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputAddress">Merk & Jenis Motor</label>
                        <input type="text" class="form-control" id="inputAddress" value="<?php echo $items->merk ?>" placeholder="Yamaha V-xion / Honda CBR DLL" name="merk">
                    </div>
                    <div class="spacer"></div>
                    <div class="form-group col-md-4">
                        <label for="inputState">Type</label>
                        <select name="type" id="inputState" class="form-control">
                            <option value="<?php echo $items->type ?>" hidden><?php echo $items->type ?></option>
                            <option value="Manual">Manual</option>
                            <option value="Matic">Matic</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputAddress">Harga Sewa</label>
                        <input name="price" type="number" class="form-control" value="<?php echo $items->price ?>" placeholder="" name="price">
                    </div>
                    <div class="spacer"></div>
                    <div class="form-group col-md-4">
                        <label for="inputAddress">Biaya Denda</label>
                        <input type="number" class="form-control" value="<?php echo $items->overDueFee ?>" placeholder="" name="overDueFee">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputAddress">Stok</label>
                        <input type="number" class="form-control" id="inputAddress" value="<?php echo $items->stock ?>" placeholder="" name="stock">
                    </div>
                </div>


                <div class="subtitle mt-4 mb-4">
                    <minor-label>Upload Foto / Gambar</minor-label>
                </div>
                <div class="row pht">
                    <div class="col-sm-12 col-md-3">
                        <div class="box-up">
                            <img id="uppict1" class="belom" src="<?php echo base_url("images/vendor/" . $items->vendorId . '/' . $items->picture1) ?>">
                        </div>
                        <label class="upload">UPLOAD Gambar 1</label>
                        <input type="file" name="pict1" class="ngumpet-coy" onchange="takePict1(this);">
                    </div>
                    <div class="col-sm-12 col-md-3">
                        <div class="box-up">
                            <img id="uppict2" class="belom" src="<?php echo base_url("images/vendor/" . $items->vendorId . '/' . $items->picture2) ?>">
                        </div>
                        <label class="upload">UPLOAD Gambar 2</label>
                        <input type="file" name="pict2" class="ngumpet-coy" onchange="takePict2(this);">
                    </div>
                    <div class="col-sm-12 col-md-3">
                        <div class="box-up">
                            <img id="uppict3" class="belom" src="<?php echo base_url("images/vendor/" . $items->vendorId . '/' . $items->picture3) ?>">
                        </div>
                        <label class="upload">UPLOAD Gambar 3</label>
                        <input type="file" name="pict3" class="ngumpet-coy" onchange="takePict3(this);">
                    </div>
                    <div class="col-sm-12 col-md-3 ">
                        <div class="box-up">
                            <img id="uppict4" class="belom" src="<?php echo base_url("images/vendor/" . $items->vendorId . '/' . $items->picture4) ?>">
                        </div>
                        <label class="upload">UPLOAD Gambar 4</label>
                        <input type="file" name="pict4" class="ngumpet-coy" onchange="takePict4(this);">
                    </div>
                </div>
                <div class="row mt-5 mt-5">
                    <div class="col-12">
                        <button type="submit" class="btn btn-outline-primary btn-lg btn-block">SIMPAN</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script>
        function takePict1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#uppict1')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function takePict2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#uppict2')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function takePict3(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#uppict3')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function takePict4(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#uppict4')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>


</body>

</html>