<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/custom/detil-trx.css') ?>">

    <!-- GLOBAL CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/global/global.css') ?>">

    <!-- bismillah -->


</head>


<body>
	<?= isset($message) ? '<script>alert("'.$message.'")</script>' : '';?>
<form action="<?=site_url('rentalTransaction/approval')?>" method="POST">
    <div class="container">
		<input type="hidden" name="transactionId" value="<?= $transaction->transactionId;?>">
        <div class="row">
            <div class="col-8">
                <div class="box">
                    <div class="header">
                        <img src="<?php echo base_url() ?>assets/imgs/Logo-minor.svg">
                    </div>
                    <div class="isi">
                        <div class="section-1">
                            <div class="row">
                                <div class="col-3">
                                    <div class="no-trx mnr-flex-col">
                                        <minor-label-sm>No Transaksi</minor-label-sm>
                                        <minor-label-sm><span><?=$transaction->transactionId?></span></minor-label-sm>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="mulai mnr-flex-col">
                                        <minor-label-sm>Mulai Penyewaan</minor-label-sm>
                                        <minor-label-sm><span><?=$transaction->rentalDate?></span></minor-label-sm>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="selesai mnr-flex-col">
                                        <minor-label-sm>Selesai Penyewaan</minor-label-sm>
                                        <minor-label-sm><span><?=dateFormat($transaction->returnDate)?></span></minor-label-sm>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="status mnr-flex-col">
                                        <minor-label-sm>Status Pembayaran</minor-label-sm>
                                        <minor-label-sm><span><?=$transaction->status;?></span></minor-label-sm>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="section-2">
                            <div class="penyewa">
                                <div class="title">
                                    <minor-lg-title>Penyewa</minor-lg-title>
                                </div>
                                <div class="body">
                                    <div class="row">
                                        <div class="col-3">
                                            <div class="info  mnr-flex-col">
                                                <minor-label-sm><?=$transaction->fullName?></minor-label-sm>
                                                <minor-label-sm><span><?=$transaction->memberId?></span></minor-label-sm>
                                            </div>
                                        </div>
                                        <div class="col-9">
                                            <div class="awal mnr-flex-col">
                                                <minor-label-sm>Lokasi Penjemputan awal</minor-label-sm>
                                                <textarea class="kecil" name="#" id="" cols="50" rows="3" disabled>
												<?=$transaction->startPickupLocation?>
												</textarea>
                                            </div>
                                            <div class="awal mnr-flex-col">
                                                <minor-label-sm>Lokasi Penjemputan Pengembalian</minor-label-sm>
                                                <textarea class="kecil" name="#" id="" cols="50" rows="3" disabled>
												  <?=$transaction->endPickupLocation?>
												</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="line"></div>
                                    <div class="row">
                                        <div class="col-3">
                                            <div class="dokumen">
                                                <div class="title">
                                                    <minor-lg-title>Dokumen</minor-lg-title>
                                                </div>
                                                <div class="ktp mnr-flex-col">
                                                    <img id="myKtp" class="kotak" src="<?php echo base_url('images/member/' . $transaction->memberId) . '/' . $transaction->ktp ?>">
                                                    <minor-label-sm><span>Foto KTP</span></minor-label-sm>
                                                </div>
                                                <div class="bukti mnr-flex-col">
                                                    <img id="myBukti" class="kotak" src="<?php echo base_url('images/EvidenceTransfer/' . $transaction->transactionId . '/' . $transaction->proofOfPayment) ?>">
                                                    <minor-label-sm><span>Bukti Transfer</span></minor-label-sm>
                                                </div>
                                            </div>

                                            <!-- preview image -->
                                            <div id="myModalKtp" class="modalguelah">
                                                <span class="close">&times;</span>
                                                <img class="modal-content-guapokoknya" id="ktpview">
                                            </div>

                                            <div id="myModalbukti" class="modalguelah">
                                                <span class="close1">&times;</span>
                                                <img class="modal-content-guapokoknya" id="buktiview">
                                            </div>
                                            <!-- end preview image -->
                                        </div>
                                        <div class="col-9 ">
                                            <div class="flexin-dong d-flex">

                                                <div class="item">
                                                    <div class="motor">
                                                        <div class="title">
                                                            <minor-lg-title>Item Penyewaan</minor-lg-title>
                                                        </div>
                                                        <div class="thum mnr-flex-row">
                                                            <img class="kotak" src="<?php echo base_url() ?>assets/imgs/image4.png">
                                                            <div class="info mnr-flex-col">
                                                                <minor-label-sm><?=$transaction->merk?></minor-label-sm>
                                                                <minor-label-sm><span><?=rupiah($transaction->price)?></span></minor-label-sm>
                                                                <minor-label-disabled><?=$transaction->itemTotal?> Motor</minor-label-disabled>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="plat">
                                                    <!-- Button trigger modal -->
                                                    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                                                        Pilih Plat Nomor
                                                    </button> -->

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLongTitle">Pilih Plat Nomor</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                        <div class="listlah">
																		<?php foreach ($listMotor as $list): ?>
                                                                            <div class="garisin">
                                                                                <input value="<?=$list['motorId']?>" type="checkbox" name="motorIds[]">
                                                                                <label for="customCheck"><?=$list['motorId']?></label>
                                                                            </div>
																		<?php endforeach;?>
                                                                        </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
                                                                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="box">
                    <div class="title" style="margin-bottom: 0px!important">
                        <minor-lg-title >TOTAL BIAYA PENYEWAAN</minor-lg-title>
                    </div>
                    <div class="rincian" style="padding:0px">
                        <div class="secharga">
                            <div class="col-6">
                                <minor-label-sm><b>Harga Sewa :</b></minor-label-sm>
                                <minor-label-sm><b>Merk</b></minor-label-sm>
                                <minor-label-sm><b>Jumlah Sewa</b></minor-label-sm>
                                <minor-label-sm><b>Durasi Peminjaman</b></minor-label-sm>
                                <minor-label-sm>Hari</minor-label-sm>
                            </div>
                            <div class="col-6 harga">

								<minor-label-sm><?=rupiah($transaction->price)?></minor-label-sm>
								<minor-label-sm><?=$transaction->merk?></minor-label-sm>

                                <minor-label-sm><?=$transaction->itemTotal?> Motor</minor-label-sm>
								<minor-label-sm>&nbsp;</minor-label-sm>

                                <minor-label-sm><?=$transaction->duration?> hari</minor-label-sm>
                            </div>
                        </div>
                        <div class="line"></div>
                        <div class="secharga">
                            <div class="col-6">
                                <minor-label-sm><b>Total Pembayaran</b></minor-label-sm>
                            </div>
                            <div class="col-6 harga">
                                <minor-label-sm><?=rupiah($transaction->totalAmount)?></minor-label-sm>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="box">
                    <div class="title" style="margin-bottom: 10px;">
                        <minor-lg-title>Pilih Motor</minor-lg-title>
                    </div>
                  <div>
						<select class="selectpicker" multiple="true" name="motorList[]" style="width:100%;padding:10px 10px; border-radius:5px;">
							<?php foreach ($listMotor as $list): ?>
							<option value="<?=$list['motorId']?>"><?=$list['motorId']?></option>
							<?php endforeach;?>
						</select>
				  </div>
                </div>
                <div class="spacer"></div>
                <div class="savecoy">
					<button type="submit" class="btn btn-success btn-lg btn-block" id="approve" >Approve</button>
                    <button type="button" class="btn btn-outline-danger btn-lg btn-block" data-toggle="modal" data-target="#exampleModalCenter2">Reject</button>
                </div>

            </div>
        </div>

    </div>

    <!-- MODAL TOLAK LAH POKOKNYA -->

    <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle2" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle2">Berikan Alasan penolakan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="2" name="note"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-primary">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</form>
    <!-- END MODAL TOLAK LAH POKOKNYA -->

    <!-- bismillah -->

    <!-- Bootstrap core JavaScript -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src=”//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js”> </script> <script>
        // Get the modal
        var modal = document.getElementById("myModalKtp");
		$("#approve").click(function() {
			$("#exampleFormControlTextarea1").val("");
        });

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById("myKtp");
        var modalImg = document.getElementById("ktpview");
        var captionText = document.getElementById("caption");
        img.onclick = function() {
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }
    </script>

    <script>
        // Get the modal
        var modal = document.getElementById("myModalbukti");

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById("myBukti");
        var modalImg = document.getElementById("buktiview");
        var captionText = document.getElementById("caption");
        img.onclick = function() {
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close1")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        var header = document.getElementById("myDIV");
        var btns = header.getElementsByClassName("btn-tolakcoy");
        for (var i = 0; i < btns.length; i++) {
            btns[i].addEventListener("click", function() {
                var current = document.getElementsByClassName("active");
                current[0].className = current[0].className.replace(" active", "");
                this.className += " active";
            });
        }

        $(function() {
            $("select").bsMultiSelect();
        });

	

		// $('#approve').click(function(){
		// 	console.log('tes');
		// 	$("#exampleFormControlTextarea1").val("ditolak");
		// });
    </script>

</body>

</html>
