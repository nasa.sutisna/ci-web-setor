<div class="motor-section">
	<?php echo isset($message) ? '<script>alert("' . $message . '")</script>' : ''; ?>
	<div class="title">
		<minor-title>Daftar Vendor</minor-title>
		<div class="input-vendor icon2">
			<img src="<?php echo base_url() ?>assets/imgs/add.svg">
			<minor-label-sm>Tambah Baru</minor-label-sm>
		</div>
	</div>
	<div style="margin-bottom: 16px">
		<form action="" method="get">
			<table>
				<tr>
					<th>Pencarian : <input value="<?= $this->input->get('q') ?>" name="q" style="border-radius: 5px;padding:2px 10px"></th>
					<th><button style="border-radius: 5px;padding:2px 10px">Cari</button></th>
				</tr>
			</table>
		</form>
	</div>
	<div class="table">

		<table class="table table-striped">
			<thead>
				<tr>
					<th scope="col">No</th>
					<th scope="col">ID Vendor</th>
					<th scope="col">Merk</th>
					<th scope="col">Type</th>
					<th scope="col">Harga Sewa</th>
					<th scope="col">Stock</th>
					<th colspan='2' scope="col" style="text-align:center;">Action</th>

				</tr>
			</thead>
			<tbody>
				<?php $no = 1;
				foreach ($contents as $list) : ?>
					<tr>
						<td scope="row"><?= $no++ ?></td>
						<td><?= $list->vendorId ?></td>
						<td><?= $list->merk ?></td>
						<td><?= $list->type ?></td>
						<td><?= $list->price ?></td>
						<td><?= $list->stock ?></td>

						<th style="text-align:center;">
							<a href='<?php echo site_url('vendor/getUpdateVendor/' . $list->vendorId) ?>'>
								<minor-label-sm style="font-size:16px;">Update</minor-label-sm>
							</a>
						</th>
						<th style="text-align:center;">
							<!-- <a href=""><minor-label-sm style="font-size:16px;">Hapus</minor-label-sm></a> -->
							<button type="button" data-toggle="modal" data-target="#exampleModalCenter">Hapus</button>
						</th>
					</tr>
				<?php endforeach; ?>
			</tbody>
			<tr>
				<td colspan="8" style="padding: 10px">
					<?php
					echo $this->pagination->create_links();
					?>
				</td>
			</tr>
		</table>
	</div>
</div>

<!-- MODAL -->

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Konfirmasi</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="total-pembayaran" style="margin-top: -25px;margin-bottom:-25px">
					<div class="row">
						<div class="col-6 tengah">
							<minor-md-title class="total">Apakah anda yakin akan menghapus data motor ini?</minor-md-title>

						</div>

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="proses" id="ringkasan">
					<button type="button" data-dismiss="modal">BATALKAN</button>
					<!-- <button type="button" class="btn-process">HAPUS</button> -->
					<a class="btn-process" href='<?php echo site_url('vendor/deleteVendor/' . $list->vendorId) ?>'>
						<minor-label-sm style="font-size:16px;">HAPUS</minor-label-sm>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- END MODAL -->

<script>
	$(".input-vendor").click(function() {
		window.location.href = '<?php echo site_url('vendor/getInputVendor') ?>';
	});
</script>