<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/custom/emptystate.css') ?>">

    <!-- GLOBAL CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/global/global.css') ?>">

    <!-- Rating -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

    <div class="row justify-content-center" style="text-align: center; margin-top:100px;">
        <div class="col">
            <div class="empty-state" style="text-align: -webkit-center;">
                <div class="myimg" style="width: 200px">
                    <img src="<?php echo base_url() ?>assets/imgs/2668459.jpg" class="img-fluid" alt="Responsive image">
                </div>
                <div class="status">
                    <minor-title>MAAF
                        <br>
                        Dashboard Masih dalam tahap pengerjaan</minor-title>
                </div>
            </div>

        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script>
        $("#ubah").click(function() {
            window.location.href = '<?php echo site_url('/') ?>';
        });

        $(".motor-item").click(function() {
            let id = $(this).attr("id");
            window.location.href = '<?php echo site_url('motor/detail/') ?>' + id;
        });
    </script>
</body>

</html>