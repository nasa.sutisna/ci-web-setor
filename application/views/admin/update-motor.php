<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/custom/input.css') ?>">

    <!-- GLOBAL CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/global/global.css') ?>">


    <title>Input Data Motor</title>
</head>

<body>

    <div class="title">
        <minor-title>Update Data Motor</minor-title>
    </div>

    <div class="body">
        <div class="container">

            <form action="<?php echo base_url().'motor/updateMotor'; ?>" method="post" class="needs-validation" novalidate>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputAddress">ID Motor</label>
                        <input type="text" class="form-control" value="<?php echo $motor->motorId ?>" name="motorId">
                    </div>
                    <div class="spacer"></div>
                    <div class="form-group col-md-4">
                        <label for="inputState">Merk & Type Motor</label>
                        <select name="vendorId" id="inputState" class="form-control">
                            <option value="<?php echo $motor->vendorId ?>">-Pilih Merk&Type-</option>
                            <?php foreach ($items as $item) : ?>
                                <option value="<?php echo $item['vendorId']; ?>"><?php echo $item['merk']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputAddress">Kota</label>
                        <select name="city" id="inputState" class="form-control">
                            <option value="<?php echo $motor->city ?>" hidden><?php echo $motor->city ?></option>
                            <option value="Jakarta" selected>Jakarta</option>
                            <option value="Bandung">Bandung</option>
                        </select>
                    </div>
                    <div class="spacer"></div>

                    <div class="form-group col-md-4">
                        <label for="inputAddress">Warna Motor</label>

                        <input type="text" class="form-control" value="<?php echo $motor->colour ?>" id="inputAddress" name="colour">
                    </div>

                </div>
                <div class="form-row">
                    <div class="form-row col-md-4">
                        <label for="inputState">Status Motor</label>
                        <select name="status" id="inputState" class="form-control">
                        <option value="<?php echo $motor->status ?>" hidden ><?php echo $motor->status ?></option>
								<option value="rented" >Sedang Disewa</option>
								<option value="ready" >Tersedia</option>
                        </select>
                    </div>
                </div>


                <div class="row mt-5 mt-5">
                    <div class="col-12">
                        <button type="submit" class="btn btn-outline-primary btn-lg btn-block">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>

</body>

</html>