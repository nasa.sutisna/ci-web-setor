<div class="motor-section">
	<div class="title">
		<minor-title>Daftar Transaksi</minor-title>
		<!-- <div class="icon2">
									<img src="<?php echo base_url() ?>assets/imgs/add.svg">
									<minor-label-sm>Tambah Baru</minor-label-sm>
								</div> -->
	</div>
	<div style="margin-bottom: 16px">
		<form action="" method="get">
			<table>
				<tr>
					<th>Pencarian : <input value="<?= $this->input->get('q') ?>" name="q" style="border-radius: 5px;padding:2px 10px"></th>
					<th><button style="border-radius: 5px;padding:2px 10px">Cari</button></th>
				</tr>
			</table>
		</form>
	</div>
	<div class="table">
		<?php if($this->session->flashdata('item') != null) :?>
			<div  class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Sukses !</strong> <?php $msg = $this->session->flashdata('item'); echo $msg['message']?>
		</div>
		<?php endif;?>

		<table class="table table-striped">
			<thead>
				<tr>
					<th scope="col">No</th>
					<th scope="col">ID Transaksi</th>
					<th scope="col">Merk</th>
					<th scope="col">Total Sewa</th>
					<th scope="col">Tgl Sewa</th>
					<th scope="col">Tgl Kembali</th>
					<th scope="col">Status</th>
					<th scope="col" style="text-align:center;">Action</th>

										</tr>
									</thead>
									<tbody>
										<?php $no=1; foreach($contents as $list): ?>
										<tr>
											<td scope="row"><?= $no++ ?></td>
											<td><?= $list->transactionId ?></td>
											<td><?= $list->merk ?></td>
											<td><?= $list->itemTotal ?></td>
											<td><?= $list->rentalDate ?></td>
											<td><?= $list->returnDate ?></td>
											<td><?= $list->status ?></td>
								
											<th style="text-align:center;">
												<a href="<?php echo site_url('rentalTransaction/getDetailTransaction/'.$list->transactionId) ?>"><minor-label-sm style="font-size:16px;">Detail</minor-label-sm></a>
											</th>
										</tr>
										<?php endforeach;?>
									</tbody>
									<tr>
										<td colspan="8" style="padding: 10px">
										<?php 
											echo $this->pagination->create_links();
										?>
										</td>
									</tr>
								</table>
							</div>
						</div>
