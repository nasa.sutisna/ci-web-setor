<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/custom/sewa.css') ?>">

    <!-- GLOBAL CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/global/global.css') ?>">

    <!-- Rating -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

    <minor-title class="tinjau">Tinjau Pesanan Anda</minor-title>

    <div class="sectinjau">
        <div class="secgrid">

            <div class="col-4">
                <minor-label>Detail Motor</minor-label>
                <div class="secmotor">
                    <div class="thumbnail">
                        <img src="<?php echo base_url("images/vendor/" . $item->vendorId . '/' . $item->picture1) ?>">
                    </div>
                    <div class="desc">
                        <minor-md-title class="col-12"><?= $item->merk ?></minor-md-title>
                        <div class="space"></div>

                        <div class="descmotor">
                            <minor-label-sm class="col-1">transmisi</minor-label-sm>
                            <div class="titik">
                                <minor-label-sm class="col-1">:</minor-label-sm>
                            </div>
                            <minor-label-sm class="col-10 isi"><?= $item->type ?></minor-label-sm>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-4">
                <minor-label>Detail Tanggal</minor-label>

                <div class="sectanggal">
                    <div class="col-6 keterangan">
                        <minor-label-sm>Mulai</minor-label-sm>
                        <minor-label-sm class="tanggal"><?= dateFormat($this->input->get('startDate')) ?></minor-label-sm>
                    </div>
                    <div class="col-6 keterangan">
                        <minor-label-sm>Sampai</minor-label-sm>
                        <minor-label-sm class="tanggal"><?= dateFormat($this->input->get('endDate')) ?></minor-label-sm>
                    </div>
                </div>
            </div>

            <div class="col-4">
                <minor-label>Detail Harga</minor-label>
                <div class="rincian">

                    <div class="secharga">
                        <div class="col-6">
                            <minor-label-sm><b>Harga Sewa Motor :</b></minor-label-sm>
                            <minor-label-sm><?= $item->merk ?></minor-label-sm>
                            <minor-label-sm><b>Jumlah Sewa</b></minor-label-sm>
                            <minor-label-sm><b>Durasi Peminjaman</b></minor-label-sm>
                            <minor-label-sm>Hari</minor-label-sm>
                        </div>
                        <div class="col-6 harga">
                            <minor-label-sm>&nbsp;</minor-label-sm>
                            <minor-label-sm><?= rupiah($item->price); ?> per hari</minor-label-sm>
                            <minor-label-sm><?= $totalSewa; ?> Motor</minor-label-sm>
                            <minor-label-sm>&nbsp;</minor-label-sm>
                            <minor-label-sm><?= $duration ?></minor-label-sm>
                        </div>
                    </div>
                    <div class="devider"></div>
                    <div class="secharga">
                        <div class="col-6">
                            <minor-label-sm><b>Total Pembayaran</b></minor-label-sm>
                        </div>
                        <div class="col-6 harga">
                            <minor-label-sm><?= rupiah(($item->price) * $totalSewa * $duration); ?></minor-label-sm>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="section-bawah">
        <div class="row">
            <div class="col-5">
                <minor-title class="tinjau">Detail Pemesanan</minor-title>
                <div class="sec-detailpem">
                    <form id="formModal" class="needs-validation" novalidate>

                        <input class="ket" type="hidden" name='memberId' value="<?= $member->memberId ?>">
                        <input class="ket" type="hidden" name='vendorId' value="<?= $item->vendorId ?>">
                        <input class="ket" type="hidden" name='rentalDate' value="<?= dateFormat($this->input->get('startDate')) ?>">
                        <input class="ket" type="hidden" name='returnDate' value="<?= dateFormat($this->input->get('endDate')) ?>">
                        <input class="ket" type="hidden" name='duration' value="<?= $duration ?>">
                        <input class="ket" type="hidden" name='total' value="<?= ($item->price) * $duration * $totalSewa ?>">
                        <input class="ket" type="hidden" name='itemTotal' value="<?= $totalSewa ?>">

                        <div class="section-isipem">
                            <minor-lg-title class="heading"><span class="minor-danger">*</span> Nama :</minor-lg-title>
                            <input class="ket" type="text" value="<?= $member->fullName ?>" placeholder="Masukan Nama Lengkap">
                            <minor-label-disabled>Isi sesuai KTP/Paspor/SIM (tanpa tanda baca dan gelar</minor-label-disabled>
                        </div>

                        <div class="section-isipem">
                            <minor-lg-title class="heading"><span class="minor-danger">*</span> Nomor Telepon :</minor-lg-title>
                            <input class="ket" type="text" value="<?= $member->phoneNumber ?>" placeholder="Contoh, 081234567890">
                        </div>

                        <div class="section-isipem">
                            <minor-lg-title class="heading"><span class="minor-danger">*</span> Alamat Email :</minor-lg-title>
                            <input class="ket" type="text" value="<?= $member->email ?>" placeholder="Masukan Email anda">
                            <minor-label-disabled>Detail Pemesanan akan di kirimkan ke email</minor-label-disabled>
                        </div>

                        <div class="section-isipem">
                            <minor-lg-title class="heading"><span class="minor-danger">*</span> Lokasi Pengambilan :</minor-lg-title>
                            <input class="ket" name='startPickup' type="text">
                            <minor-label-disabled>Pengambilan bisa di Stasiun, Terminal, Penginapan atau langsung di tempat kami</minor-label-disabled>
                        </div>

                        <div class="section-isipem">
                            <minor-lg-title class="heading"><span class="minor-danger">*</span> Lokasi Pengembalian :</minor-lg-title>
                            <input class="ket" name='endPickup' type="text">
                            <minor-label-disabled>Pengembalian bisa di Stasiun, Terminal, Penginapan atau langsung di tempat kami</minor-label-disabled>
                        </div>

                        <div class="section-isipem">
                            <minor-lg-title class="heading">Permintaan Khusus (opsional) :</minor-lg-title>
                            <textarea name="notes" id="" cols="1" rows="3"></textarea>
                        </div>
                        <div>
                            <minor-label-disabled> Catatan : <span class="minor-danger">*</span> (tidak boleh kosong)</minor-label-disabled>
                        </div>

                </div>
            </div>

            <div class="col-7">
                <minor-title class="tinjau">Kebijakan Pembatalan Pemesanan</minor-title>
                <div class="sec-kebijakan">
                    <div class="isi">
                        <div class="gambar">
                            <img src="<?php echo base_url() ?>assets/imgs/kebijakan.svg">
                        </div>
                        <div class="keterangan">
                            <minor-label-sm>
                                Apabila anda ingin melakukan pembatalan pesanan, dapat dilakukan 1 x 24 jam setelah proses booking. Pembatalan pesanan tidak dapat diproses setelah transaksi pembayaran dilakukan
                            </minor-label-sm>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer">
                <button type="button" data-toggle="modal" data-target="#exampleModalCenter">Lanjut Pembayaran</button>
            </div>
            <!-- </form> -->
        </div>
    </div>


    <!-- MODAL -->

    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Metode Pembayaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="total-pembayaran" style="margin-top: -25px;margin-bottom:-25px">
                        <div class="row">
                            <div class="col-6 kiri">
                                <minor-md-title class="total">Total Pembayaran</minor-md-title>
                                <minor-md-title class="rupiah"><?= rupiah(($item->price) * $totalSewa * $duration); ?></minor-md-title>
                            </div>

                        </div>
                    </div>
                    <div class="pembayaran">
                        <div class="title">
                            <minor-md-title>Pembayaran Yang Tersedia</minor-md-title>
                        </div>
                        <div class="list">
                            <div class="row">
                                <div class="col-10">
                                    <div class="logo">
                                        <img src="<?php echo base_url() ?>assets/imgs/bca.png">
                                        <minor-md-title>BANK BCA</minor-md-title>
                                    </div>
                                    <div class="noreq">
                                        <minor-md-title> Atas Nama : Minor - 1029384576 </minor-md-title>
                                    </div>
                                </div>
                                <div class="col-2 arrow" hidden>
                                    <img src="<?php echo base_url() ?>assets/imgs/arrow-right.svg">
                                </div>
                            </div>
                        </div>
                        <div class="list">
                            <div class="row">
                                <div class="col-10">
                                    <div class="logo">
                                        <img src="<?php echo base_url() ?>assets/imgs/mandiri.png">
                                        <minor-md-title>BANK MANDIRI</minor-md-title>
                                    </div>
                                    <div class="noreq">
                                        <minor-md-title> Atas Nama : Minor - 1029384576 </minor-md-title>
                                    </div>
                                </div>
                                <div class="col-2 arrow" hidden>
                                    <img src="<?php echo base_url() ?>assets/imgs/arrow-right.svg">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="proses" id="ringkasan">
                        <!-- <minor-title >PROSES PEMBAYARAN</minor-title> -->
                        <button type="submit" class="btn-process">PROSES PEMBAYARAN</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- END MODAL -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>

    <script>
        // $("#ringkasan").click(function() {
        //     window.location.href = '<?php echo site_url('rentalTransaction/detail') ?>';
        // });
        $('#formModal').submit(function(e) {

            var form = $(this);

            e.preventDefault();

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('rentalTransaction/checkout'); ?>",
                data: form.serialize(),
                dataType: "json",
                success: function(response) {
                    window.location.href = '<?php echo site_url('rentalTransaction/getUploadTransfer/') ?>' + response.transactionId;
                },
                error: function(error) {
                    const response = JSON.parse(error.responseText);
                    alert(response.message);
                }
            });

        });
    </script>

</body>

</html>