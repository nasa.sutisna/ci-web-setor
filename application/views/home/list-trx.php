<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/custom/list-trx.css') ?>">

    <!-- GLOBAL CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/global/global.css') ?>">


    <title>Registration</title>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-1">
                <div class="menu">
                    <ul class="nav nav-pills mb-4" id="pills-tab" role="tablist">
                        <!-- LIST MENU SIDEBAR -->
                        <li class="nav-item">
                            <img src="<?php echo base_url() ?>assets/imgs/profile.svg" alt="">
                            <a class="nav-link active" id="pills-Profile-tab" data-toggle="pill" href="#pills-Profile" role="tab" aria-controls="pills-Profile" aria-selected="true">Profil</a>
                        </li>
                        <li class="nav-item">
                            <img src="<?php echo base_url() ?>assets/imgs/transaksi.svg" alt="">
                            <a class="nav-link" id="pills-Transaksi-tab" data-toggle="pill" href="#pills-Transaksi" role="tab" aria-controls="pills-Transaksi" aria-selected="false">Transaksi</a>
                        </li>
                        <!-- END LIST MENU SIDEBAR -->
                    </ul>
                </div>
            </div>
            <div class="garisku col-1"></div>
            <div class="col-10 content">
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-Profile" role="tabpanel" aria-labelledby="pills-Profile-tab">

                        <!-- profile section -->
                        <div class="profile">
                            <div class="photo">
                                <img src="<?php echo base_url("images/member/" . $this->session->userdata('memberId') . '/' . $this->session->userdata('photo')) ?>" alt="">
                            </div>
                            <div class="keterangan">
                                <minor-medium><?php echo $this->session->userdata('fullName'); ?></minor-medium>
                                <minor-md-title><?php echo $this->session->userdata('memberId'); ?></minor-md-title>
                            </div>
                        </div>

                        <div class="section-edit">

                            <form class="audah" action="<?php echo base_url().'member/updateMember'; ?>" method="post" enctype="multipart/form-data" class="needs-validation">

                                <div class="row atasinelah">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="inputNama4">No. KTP </label>
                                            <input type="text" class="form-control" id="inputNama3" placeholder="Masukan No. KTP" value="<?php echo $this->session->userdata('memberId'); ?>" name='id'>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="inputNama4">Nama</label>
                                            <input type="text" class="form-control" id="inputNama4" placeholder="Masukan Nama" value="<?php echo $this->session->userdata('fullName'); ?>" name='name'>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="inputAddress">Alamat</label>
                                        <textarea class="form-control" id="inputAddress" rows="2" name='address'><?php echo $this->session->userdata('address'); ?></textarea> 
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="inputEmail4">Email</label>
                                            <input type="text" class="form-control" id="inputEmail4" placeholder="Masukan Email" value="<?php echo $this->session->userdata('email'); ?>" disabled>
                                            <input type="hidden" value="<?php echo $this->session->userdata('email'); ?>" name='email'>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group col-md-2 turundong">
                                        <label for="inputKecamatan">Kecamatan</label>
                                        <input type="text" class="form-control" id="inputKecamantan" placeholder="Kecamatan" value="Pondok Aren">
                                    </div>
                                    <div class="form-group col-md-2 turundong">
                                        <label for="inputKota">Kota</label>
                                        <input type="text" class="form-control" id="inputKota" placeholder="Kota" value="jakarta">
                                    </div>
                                    <div class="form-group col-md-2 turundong">
                                        <label for="inputKodePos">Kode Pos</label>
                                        <input type="text" class="form-control" id="inputKodePos" placeholder="kodepos" value="15413">
                                    </div> -->

                                    <div class="form-group col-md-6">
                                        <label for="inputPhone">Nomor Telpon</label>
                                        <input type="number" class="form-control" id="inputPhone" placeholder="masukan no telp" value="<?php echo $this->session->userdata('phoneNumber'); ?>" name='phone'>
                                    </div>
                                </div>

                                <!-- Button trigger modal -->

                                <div class="row ketinggian">
                                    <div class="col-3">
                                        <div class="btn-mnr-outline-success" data-toggle="modal" data-target="#exampleModal">
                                            Ubah Password
                                        </div>
                                    </div>
                                </div>

                                <div class="row atasinelah">
                                    <div class="col-12">
                                        <button type="submit" class="btn-mnr-sukses2">SIMPAN</button>
                                    </div>
                                </div>
                            </form>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="<?php echo base_url().'member/updatePassword'; ?>" method="post">
                                                <input type="hidden" value="<?php echo $this->session->userdata('email'); ?>" name='email'>
                                                <div class="form-section">
                                                    <p>Password</p>
                                                    <div class="pass">
                                                        <input id="passwordd" type="password" name="password" placeholder="Enter Your Password">
                                                        <img onclick="pass()" src="<?php echo base_url() ?>assets/imgs/mata.svg" alt="">
                                                    </div>
                                                </div>
                                                <div class="form-section">
                                                    <p>Confirm Password</p>
                                                    <div class="pass">
                                                        <input id="confirm" type="password" name="password_confirm" placeholder="Enter Your Password">
                                                        <img onclick="passconf()" src="<?php echo base_url() ?>assets/imgs/mata.svg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="ma-btn btn-success">Reset Password</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                        </div>





                        <!-- end profile section -->



                    </div>
                    <div class="tab-pane fade" id="pills-Transaksi" role="tabpanel" aria-labelledby="pills-Transaksi-tab">
                        <div class="isi">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Menunggu Pembayaran</a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Sudah Dibayar</a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Transaksi Selesai</a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

                                    <!-- Check transaction NOT PAY is null -->
                                    <?php if ($notpay == null) { ?>
                                        <!-- EMPTY STATE -->
                                        <div class="empty-state">
                                            <img src="<?php echo base_url() ?>assets/imgs/empty.jpg" alt="">
                                            <minor-medium>
                                                Ooops, Belum ada transaksi
                                                <br>
                                            </minor-medium>
                                            <minor-medium>
                                                <span>Ayo..!! lakukan transaksi sewa motor sekarang juga..</span>
                                            </minor-medium>
                                        </div>
                                        <!-- END EMPTY STATE -->
                                    <?php } ?>
                                    <!-- NG FOR DI SINI -->
                                    <?php foreach ($notpay as $notpay) : ?>
                                        <div class="box-list">
                                            <div class="box-header">
                                                Transaksi Penyewaan
                                            </div>
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-4 no-trx">
                                                        <minor-label-sm>No transaksi penyewaan</minor-label-sm>
                                                        <minor-label-sm><span><?php echo $notpay['transactionId']; ?></span></minor-label-sm>
                                                    </div>
                                                    <div class="col-2 status">
                                                        <minor-label-sm>Status</minor-label-sm>
                                                        <minor-label-sm><span><?php echo $notpay['status']; ?></span></minor-label-sm>
                                                    </div>
                                                    <?php if ($notpay['status'] == 'ORDER') { ?>
                                                        <div class="col-5 may-btn">
                                                            <a href="<?php echo site_url('rentalTransaction/getUploadTransfer/' . $notpay['transactionId']) ?>" class="btn-mnr-outline-danger">Selesaikan Pembayaran</a>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2 col-sm-12 photo">
                                                        <img src="<?php echo base_url() ?>images/vendor/<?php echo $notpay['vendorId']; ?>/<?php echo $notpay['picture1']; ?>" alt="">
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 ket">
                                                        <minor-label-sm><?php echo $notpay['merk']; ?></minor-label-sm>
                                                        <minor-label-sm><span><?php echo rupiah($notpay['totalAmount']); ?></span></minor-label-sm>
                                                    </div>
                                                    <div class="garis"></div>
                                                    <div class="col-md-6 col-sm-12 tgl">
                                                        <div class="mulai">
                                                            <minor-label-sm>Mulai Peminjaman</minor-label-sm>
                                                            <minor-label-sm><span><?php echo $notpay['rentalDate']; ?></span></minor-label-sm>
                                                        </div>
                                                        <div class="garis-tgl"></div>
                                                        <div class="selesai">
                                                            <minor-label-sm>Selesai Peminjaman</minor-label-sm>
                                                            <minor-label-sm><span><?php echo $notpay['returnDate']; ?></span></minor-label-sm>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                    <!-- END NG FOR DISINI -->

                                    <!-- EMPTY STATE -->
                                    <!-- <div class="empty-state">
                                        <img src="<?php echo base_url() ?>assets/imgs/empty.jpg" alt="">
                                        <minor-medium>
                                            Kamu Keren... Semua transaksi di bayar tepat waktu
                                            <br>
                                        </minor-medium>
                                        <minor-medium>
                                            <span>Atau kamu belum melakukan Penyewaan???</span>
                                        </minor-medium>
                                    </div> -->
                                    <!-- END EMPTY STATE -->


                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <!-- Check transaction PAY is null -->
                                    <?php if ($pay == null) { ?>
                                        <!-- EMPTY STATE -->
                                        <div class="empty-state">
                                            <img src="<?php echo base_url() ?>assets/imgs/empty.jpg" alt="">
                                            <minor-medium>
                                                Ooops, Belum ada transaksi yang sudah dibayar
                                                <br>
                                            </minor-medium>
                                            <minor-medium>
                                                <span>Ayo..!! lakukan transaksi sewa motor dan lakukan pembayran sekarang juga..</span>
                                            </minor-medium>
                                        </div>
                                        <!-- END EMPTY STATE -->
                                    <?php } ?>

                                    <!-- NG FOR DI SINI -->
                                    <?php foreach ($pay as $pay) : ?>
                                        <div class="box-list">
                                            <div class="box-header">
                                                Transaksi Penyewaan
                                            </div>
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-4 no-trx">
                                                        <minor-label-sm>No transaksi penyewaan</minor-label-sm>
                                                        <minor-label-sm><span><?php echo $pay['transactionId']; ?></span></minor-label-sm>
                                                    </div>
                                                    <div class="col-2 status">
                                                        <minor-label-sm>Status</minor-label-sm>
                                                        <minor-label-sm><span><?php echo $pay['status']; ?></span></minor-label-sm>
                                                    </div>
                                                    <?php if ($pay['status'] == 'PAYMENT') { ?>
                                                        <div class="col-5 may-btn">
                                                        <div class="btn-mnr-outline-danger">Menunggu Verifikasi Admin</div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2 col-sm-12 photo">
                                                        <img src="<?php echo base_url() ?>images/vendor/<?php echo $pay['vendorId']; ?>/<?php echo $pay['picture1']; ?>" alt="">
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 ket">
                                                        <minor-label-sm><?php echo $pay['merk']; ?></minor-label-sm>
                                                        <minor-label-sm><span><?php echo rupiah($pay['totalAmount']); ?></span></minor-label-sm>
                                                    </div>
                                                    <div class="garis"></div>
                                                    <div class="col-md-6 col-sm-12 tgl">
                                                        <div class="mulai">
                                                            <minor-label-sm>Mulai Peminjaman</minor-label-sm>
                                                            <minor-label-sm><span><?php echo $pay['rentalDate']; ?></span></minor-label-sm>
                                                        </div>
                                                        <div class="garis-tgl"></div>
                                                        <div class="selesai">
                                                            <minor-label-sm>Selesai Peminjaman</minor-label-sm>
                                                            <minor-label-sm><span><?php echo $pay['returnDate']; ?></span></minor-label-sm>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                    <!-- END NG FOR DISINI -->
                                </div>
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                    <!-- Check transaction FINISH is null -->
                                    <?php if ($finish == null) { ?>
                                        <!-- EMPTY STATE -->
                                        <div class="empty-state">
                                            <img src="<?php echo base_url() ?>assets/imgs/empty.jpg" alt="">
                                            <minor-medium>
                                                Ooops, Belum ada transaksi yang selesai
                                                <br>
                                            </minor-medium>
                                            <minor-medium>
                                                <span>Ayo..!! selesaikan transaksi mu sekarang juga..</span>
                                            </minor-medium>
                                        </div>
                                    <?php } ?>
                                    <!-- END EMPTY STATE -->
                                    <!-- NG FOR DI SINI -->
                                    <?php foreach ($finish as $finish) : ?>
                                        <div class="box-list">
                                            <div class="box-header">
                                                Transaksi Penyewaan
                                            </div>
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-4 no-trx">
                                                        <minor-label-sm>No transaksi penyewaan</minor-label-sm>
                                                        <minor-label-sm><span><?php echo $finish['transactionId']; ?></span></minor-label-sm>
                                                    </div>
                                                    <div class="col-2 status">
                                                        <minor-label-sm>Status</minor-label-sm>
                                                        <minor-label-sm><span><?php echo $finish['status']; ?></span></minor-label-sm>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2 col-sm-12 photo">
                                                        <img src="<?php echo base_url() ?>images/vendor/<?php echo $finish['vendorId']; ?>/<?php echo $finish['picture1']; ?>" alt="">
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 ket">
                                                        <minor-label-sm><?php echo $finish['merk']; ?></minor-label-sm>
                                                        <minor-label-sm><span><?php echo rupiah($finish['totalAmount']); ?></span></minor-label-sm>
                                                    </div>
                                                    <div class="garis"></div>
                                                    <div class="col-md-6 col-sm-12 tgl">
                                                        <div class="mulai">
                                                            <minor-label-sm>Mulai Peminjaman</minor-label-sm>
                                                            <minor-label-sm><span><?php echo $finish['rentalDate']; ?></span></minor-label-sm>
                                                        </div>
                                                        <div class="garis-tgl"></div>
                                                        <div class="selesai">
                                                            <minor-label-sm>Selesai Peminjaman</minor-label-sm>
                                                            <minor-label-sm><span><?php echo $finish['returnDate']; ?></span></minor-label-sm>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                    <!-- END NG FOR DISINI -->
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>




        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
        <!-- My Script -->
        <script>
            function pass() {
                var x = document.getElementById("passwordd");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            }

            function passconf() {
                var x = document.getElementById("confirm");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            }
        </script>





</body>

</html>