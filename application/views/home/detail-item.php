<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/custom/detail-item.css') ?>">

    <!-- GLOBAL CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/global/global.css') ?>">

    <!-- Rating -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-sm-12">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="item-image">
                            <div class="display-img">
                                <div id="imgtext"></div>
                                <img src="<?php echo base_url("images/vendor/" . $item->vendorId . '/' . $item->picture1) ?>" id="expandedImg">
                            </div>
                            <div class="thumbnail-img">
                                <img class="1" src="<?php echo base_url("images/vendor/" . $item->vendorId . '/' . $item->picture1) ?>" onclick="myFunction(this);">
                                <img class="2" src="<?php echo base_url("images/vendor/" . $item->vendorId . '/' . $item->picture2) ?>" onclick="myFunction(this);">
                                <img class="3" src="<?php echo base_url("images/vendor/" . $item->vendorId . '/' . $item->picture3) ?>" onclick="myFunction(this);">
                                <img class="4" src="<?php echo base_url("images/vendor/" . $item->vendorId . '/' . $item->picture4) ?>" onclick="myFunction(this);">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="item-info">
                            <minor-title><?= $item->merk; ?></minor-title>
                            <div class="desc-item">
                                <div class="kota">
                                    <img src="<?php echo base_url() ?>assets/imgs/lokasi.svg" alt="">
                                    <minor-label-sm><?= $item->city; ?></minor-label-sm>
                                </div>
                                <div class="gear">
                                    <img src="<?php echo base_url() ?>assets/imgs/gear.svg" alt="">
                                    <minor-label-sm><?= $item->type; ?></minor-label-sm>
                                </div>
                            </div>
                            <div class="rating">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star"></span>
                                <span class="fa fa-star"></span>
                                <div class="total">
                                    <minor-label-sm>&#40;10&#41;</minor-label-sm>
                                </div>
                            </div>
                            <div class="harga">
                                <minor-title><?= rupiah($item->price) ?></minor-title>
                            </div>
                            <div class="stock-ready">
                                Stok Tersedia : <?= count($relatedMotor); ?>
                            </div>
                            <div style="display: inline-flex;">
                                <div class="input-group">
                                    <input type="button" value="-" class="button-minus" data-field="quantity">
                                    <input type="number" id="quantity" step="1" max="<?= count($relatedMotor); ?>" value="1" name="quantity" class="quantity-field">
                                    <input type="button" value="+" class="button-plus" data-field="quantity">
                                </div>
                            </div>
                            <div class="jumlah">
                                <div style="width: 100px;line-height: 65px;">Jumlah Sewa</div>
                            </div>
                            <div class="tombol-action">
                                <button class="cs" type="button">HUBUNGI CS</button>
                                <button class="sewa" data="<?= '?startDate=' . $this->input->get('startDate') . '&endDate=' . $this->input->get('endDate'); ?>" type="button" id="<?= $item->vendorId; ?>">SEWA</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-1">
                <div class="section-list-item">
                    <div class="box">
                        <div class="title-right">Motor Lainnya </div>
                        <div class="list-item">
                            <?php $no = 0;
                            foreach ($listVendor as $vendor) : ?>
                                <?php if ($vendor['vendorId'] != $item->vendorId) : ?>
                                    <div class="item listRight" id="list0" data="<?= $vendor['vendorId']; ?>">
                                        <div class="item-thumbnail">
                                            <img src="<?php echo base_url("images/vendor/" . $vendor['vendorId'] . '/' . $vendor['picture1']) ?>" alt="">
                                        </div>
                                        <div class="info">
                                            <minor-md-title><?= $vendor['merk']; ?></minor-md-title>
                                            <div class="rating custom">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <div class="total">
                                                    <minor-label-sm>&#40;10&#41;</minor-label-sm>
                                                </div>
                                            </div>
                                            <minor-md-title class="Harga"><?= rupiah($vendor['price']); ?></minor-md-title>
                                        </div>
                                    </div>
                            <?php endif;
                            endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="container">

                <div class="tombolhp">
                    <button class="cs" type="button">HUBUNGI CS</button>
                    <button class="sewa" data="<?= '?startDate=' . $this->input->get('startDate') . '&endDate=' . $this->input->get('endDate'); ?>" type="button" id="<?= $item->vendorId; ?>">SEWA</button>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col">
                <!-- SEGMENT DESC & ULASAN -->

                <div class="segment">


                    <ul class="nav nav-pills mb-4" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-Deskripsi-tab" data-toggle="pill" href="#pills-Deskripsi" role="tab" aria-controls="pills-Deskripsi" aria-selected="true">Deskripsi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-Ulasan-tab" data-toggle="pill" href="#pills-Ulasan" role="tab" aria-controls="pills-Ulasan" aria-selected="false">Ulasan</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">

                        <!-- SEGMENT DESC -->

                        <div class="tab-pane fade show active" id="pills-Deskripsi" role="tabpanel" aria-labelledby="pills-Deskripsi-tab">
                            <div class="desc">
                                <minor-md-title class="col-1">Nama</minor-md-title>
                                <div class="titik">
                                    <minor-md-title class="col-1">:</minor-md-title>
                                </div>
                                <minor-md-title class="col-10"><?= $item->merk ?></minor-md-title>
                            </div>


                            <div class="desc">
                                <minor-md-title class="col-1">transmisi</minor-md-title>
                                <div class="titik">
                                    <minor-md-title class="col-1">:</minor-md-title>
                                </div>
                                <minor-md-title class="col-10"><?= $item->type ?></minor-md-title>
                            </div>
                        </div>

                        <!-- END SEGMENT DESC -->

                        <!-- SEGMENT ULASAN -->

                        <div class="tab-pane fade" id="pills-Ulasan" role="tabpanel" aria-labelledby="pills-Ulasan-tab">
                            <div class="tab-ulasan">

                                <div class="ulasan-user">
                                    <!-- ngfor disini -->
                                    <div class="atas">
                                        <div class="col-1">
                                            <img src="<?php echo base_url() ?>assets/imgs/gem.jpg">
                                        </div>
                                        <div class="col-7 infome">
                                            <minor-label-sm class="name">Katty Martin</minor-label-sm>
                                            <minor-label-sm class="date">17 Oktober 2019</minor-label-sm>
                                        </div>
                                        <div class="col-4 myrate">
                                            <div class="rating">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="isi col-12">
                                        <minor-label-sm class="name">Motornya bersih terawat.. masih kenceng juga masih ada tarikannya buat nanjak.. pokoknya debest banget deh di sini, harus coba seriusan!!!, kalian gk akan nyesel kalo sewa di sini.. aman banget dan ramah orangnya</minor-label-sm>
                                    </div>
                                    <div class="devider"></div>
                                </div>

                                <div class="ulasan-user">
                                    <div class="atas">
                                        <div class="col-1">
                                            <img src="<?php echo base_url() ?>assets/imgs/feri.jfif">
                                        </div>
                                        <div class="col-7 infome">
                                            <minor-label-sm class="name">Feri M Irpan</minor-label-sm>
                                            <minor-label-sm class="date">17 Desember 2019</minor-label-sm>
                                        </div>
                                        <div class="col-4 myrate">
                                            <div class="rating">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="isi col-12">
                                        <minor-label-sm class="name">Backendnya mantep nihhhh kerenlah... lahh gueee gitu loh!!! &#x1F60E;</minor-label-sm>
                                    </div>
                                    <div class="devider"></div>
                                </div>

                                <div class="ulasan-user">
                                    <div class="atas">
                                        <div class="col-1">
                                            <img src="<?php echo base_url() ?>assets/imgs/nindia.jfif">
                                        </div>
                                        <div class="col-7 infome">
                                            <minor-label-sm class="name">Nindia Herdiani A</minor-label-sm>
                                            <minor-label-sm class="date">17 Desember 2019</minor-label-sm>
                                        </div>
                                        <div class="col-4 myrate">
                                            <div class="rating">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="isi col-12">
                                        <minor-label-sm class="name">Siapa dulu analyst sistemnya... Guaa gitu....!! sombong amat lu pada &#x1F60F;</minor-label-sm>
                                    </div>
                                    <div class="devider"></div>
                                </div>

                                <div class="ulasan-user">
                                    <div class="atas">
                                        <div class="col-1">
                                            <img src="<?php echo base_url() ?>assets/imgs/fattah.jfif">
                                        </div>
                                        <div class="col-7 infome">
                                            <minor-label-sm class="name">Fattah Nur R</minor-label-sm>
                                            <minor-label-sm class="date">17 Desember 2019</minor-label-sm>
                                        </div>
                                        <div class="col-4 myrate">
                                            <div class="rating">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="isi col-12">
                                        <minor-label-sm class="name">Enak liatnya? kaku gak? gak kan? iyalah... Guaaa UI/UX desinger + Engineernya.. Tapi maap responsivenya masih ancur untuk hp waktunya mepet &#x1F61D;</minor-label-sm>
                                    </div>
                                    <div class="devider"></div>
                                </div>

                                <div class="ulasan-user">
                                    <div class="atas">
                                        <div class="col-1">
                                            <img src="<?php echo base_url() ?>assets/imgs/nasa.jfif">
                                        </div>
                                        <div class="col-7 infome">
                                            <minor-label-sm class="name">Nasa Sutisna</minor-label-sm>
                                            <minor-label-sm class="date">17 Desember 2019</minor-label-sm>
                                        </div>
                                        <div class="col-4 myrate">
                                            <div class="rating">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="isi col-12">
                                        <minor-label-sm class="name">Aduhhhh ada apa sih kalian semua... beruntung kalian punya gue.. fullstack developer.. hehhehe maap sombong, sekali kali boleh lah... &#x1F605; 41516310005 pak.. ehehe</minor-label-sm>
                                    </div>
                                    <div class="devider"></div>
                                </div>

                            </div>
                        </div>
                        <!-- END SEGMENT ULASAN -->

                    </div>
                </div>
            </div>
        </div>
    </div>





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script>
        $(".sewa").click(function() {
            let id = $(this).attr("id");
            let date = $(this).attr("data");
            let quantity = document.getElementById('quantity').value;

            window.location.href = '<?php echo site_url('motor/sewa/') ?>' + id + date + '&quantity=' + quantity;
        });

        function incrementValue(e) {
            e.preventDefault();
            var input = document.getElementById('quantity');
            var max = input.getAttribute("max");
            var fieldName = $(e.target).data('field');
            var parent = $(e.target).closest('div');

            var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

            if (!isNaN(currentVal) && currentVal < max) {
                parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
            } else if (currentVal == max) {
                parent.find('input[name=' + fieldName + ']').val(currentVal);
            } else {
                parent.find('input[name=' + fieldName + ']').val(0);
            }
        }

        function decrementValue(e) {
            e.preventDefault();
            var fieldName = $(e.target).data('field');
            var parent = $(e.target).closest('div');
            var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

            if (!isNaN(currentVal) && currentVal > 0) {
                parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
            } else {
                parent.find('input[name=' + fieldName + ']').val(0);
            }
        }

        $('.input-group').on('click', '.button-plus', function(e) {
            incrementValue(e);
        });

        $('.input-group').on('click', '.button-minus', function(e) {
            decrementValue(e);
        });

        $('[id^=list0]').each(function(i) {
            i++;
            $(this).click(function() {
                var id = $(this).attr('data');
                var urlParams = new URLSearchParams(window.location.search);
                console.log(urlParams.toString());
                console.log(id);

                window.location.href = '<?php echo site_url('motor/detail/') ?>' + id + '?' + urlParams;
            });
        });

        $('.cs').click(function() {
            window.open(
                'https://api.whatsapp.com/send?phone=628981234567',
                '_blank' // <- This is what makes it open in a new window.
            );
        });

        function myFunction(imgs) {
            var expandImg = document.getElementById("expandedImg");
            var imgText = document.getElementById("imgtext");
            expandImg.src = imgs.src;
            imgText.innerHTML = imgs.alt;
            expandImg.parentElement.style.display = "block";
        }
    </script>
</body>

</html>
