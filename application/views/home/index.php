<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

	<!-- Custom CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/custom/rumah.css') ?>">

	<!-- GLOBAL CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/global/global.css') ?>">





	<title>Home</title>
</head>

<body>


	<div class="banner">
		<img src="<?php echo base_url() ?>assets/imgs/banner.jpg" class="img-fluid" alt="Responsive image">
	</div>

	<div class="pencarian">
		<?php echo isset($message) ? '<script>alert("' . $message . '")</script>' : ''; ?>
		<div class="box">
			<div class="isi">
				<h3>Hey, Mau Sewa Motor?</h3>
			</div>
			<div class="pilihan">
				<form action="<?php echo site_url('motor/search') ?>" method="GET">
					<div class="lokasi">
						<h5>Lokasi</h5>
						<div class="p-lokasi">
							<img src="<?php echo base_url() ?>assets/imgs/lokasi.svg" alt="">
							<select name="city" placholder="pilih kota" value="<?php echo $this->input->get('city') ?>">
								<option value="jakarta" <?= $this->input->get('city') == 'jakarta' ? 'selected' : '' ?>>Jakarta</option>
								<option value="bandung" <?= $this->input->get('city') == 'bandung' ? 'selected' : '' ?>>Bandung</option>
							</select>
						</div>
					</div>
					<div class="transmisi">
						<h5>Transmisi</h5>
						<div class="p-lokasi">
							<img src="<?php echo base_url() ?>assets/imgs/gear.svg" alt="">
							<select name="type" placholder="pilih type">
								<option value="manual" <?= $this->input->get('type') == 'manual' ? 'selected' : '' ?>>Manual</option>
								<option value="matic" <?= $this->input->get('type') == 'matic' ? 'selected' : '' ?>>Matic</option>
							</select>
						</div>
					</div>
					<div class="garis"></div>
					<div class="date">
						<div class="tanggal-mulai">
							<h5>Tanggal Mulai</h5>
							<div class="p-lokasi">
								<img src="<?php echo base_url() ?>assets/imgs/calender.svg" alt="">
								<input data-provide="datepicker" readonly type="text" id="startDate" name="startDate" value="<?php echo $this->input->get('startDate') ?>" required="">
							</div>
						</div>
						<div class="tanggal-selesai">
							<h5>Tanggal Selesai</h5>
							<div class="p-lokasi">
								<img src="<?php echo base_url() ?>assets/imgs/calender.svg" alt="">
								<input data-provide="datepicker" id="endDate" readonly type="text" name="endDate" value="<?php echo $this->input->get('endDate') ?>" required="">
							</div>
						</div>
					</div>
			</div>
			<div class="my-btn">
				<button type="submit" value="Cari">Cari Kendaraan</button>
			</div>
			<!-- <button id="emptystate">
				temporary
			</button> -->
			</form>

		</div>
	</div>

	<!-- TAMPILAN Hp -->

	<div class="ahsudahlah">
		<div class="isi text-center">
			<h3>Hey, Mau Sewa Motor?</h3>
		</div>
		<form action="<?php echo site_url('motor/search') ?>" method="GET">
			<div class=" container">
				<div class="form-group">
					<label for="exampleFormControlSelect1">Pilih Lokasi</label>
					<select class="form-control" id="exampleFormControlSelect1" name="city" placholder="pilih kota" value="<?php echo $this->input->get('city') ?>">
						<option value="jakarta" <?= $this->input->get('city') == 'jakarta' ? 'selected' : '' ?>>Jakarta</option>
						<option value="bandung" <?= $this->input->get('city') == 'bandung' ? 'selected' : '' ?>>Bandung</option>
					</select>
				</div>
				<div class="form-group">
					<label for="exampleFormControlSelect1">Pilih Jenis Transmisi</label>
					<select class="form-control" id="exampleFormControlSelect1" name="type" placholder="pilih type">
						<option value="manual" <?= $this->input->get('type') == 'manual' ? 'selected' : '' ?>>Manual</option>
						<option value="matic" <?= $this->input->get('type') == 'matic' ? 'selected' : '' ?>>Matic</option>
					</select>
				</div>

				<div class="form-group">
					<label for="exampleFormControlSelect1">Tanggap Mulai</label>
					<input type="date" class="form-control" name="startDate" value="<?php echo $this->input->get('startDate') ?>" required="">
				</div>

				<div class="form-group">
					<label for="exampleFormControlSelect1">Tanggap Selesai</label>
					<input type="date" class="form-control" name="endDate" value="<?php echo $this->input->get('endDate') ?>" required="">
				</div>

				<button type="submit" class="btn btn-outline-primary btn-lg btn-block" value="Cari">CARI MOTOR</button>
			</div>
		</form>
	</div>

	<!-- TAMPILAN HP -->

	<div class="container">

		<div class="segmen-mengapakita">
			<div class="title mb-5">
				<minor-heading>Mengapa kita?</minor-heading>
			</div>


			<div class="responsive mb-5">
				<div class="row center">
					<div class="col-md-4 mb-5 d-flex flex-column">
						<div class="icon">
							<img src="<?php echo base_url() ?>assets/imgs/mengapa-1.svg" alt="">
						</div>
						<div class="desc d-flex flex-column mt-5 ">
							<minor-title>Pilih Bayar Dimana aja</minor-title>
							<minor-subtitle>Kamu bisa pilih tipe pembayaranmu dan bayar dimana <br> aja. Tinggal Bayar aja.</minor-subtitle>
						</div>
					</div>
					<div class="col-md-4 mb-5">
						<div class="icon">
							<img src="<?php echo base_url() ?>assets/imgs/mengapa-2.svg" alt="">
						</div>
						<div class="desc d-flex flex-column mt-5 ">
							<minor-title>Proses Cepat</minor-title>
							<minor-subtitle>Booking yg kamu buat pasti cepat diproses. Kamu bisa <br> lebih cepat melakukan perjalanan. Let's Go</minor-subtitle>
						</div>
					</div>
					<div class="col-md-4 mb-5">
						<div class="icon">
							<img src="<?php echo base_url() ?>assets/imgs/mengapa-3.svg" alt="">
						</div>
						<div class="desc d-flex flex-column mt-5 ">
							<minor-title>Transaksi Aman</minor-title>
							<minor-subtitle>Semua proses tersistem dengan baik. Pasti aman deh ! <br> EZ-Transaction, Pasti Aman !</minor-subtitle>
						</div>

					</div>
				</div>

			</div>
		</div>







		<script>
			var today = new Date().toISOString().split('T')[0];
			var startDate = document.getElementsByName("startDate")[0].setAttribute('min', today);

			$(".motor-item").click(function() {
				let id = $(this).attr("id");
				window.location.href = '<?php echo site_url('motor/detail/') ?>' + id;
			});

			$("#emptystate").click(function() {

				window.location.href = '<?php echo site_url('home/emptystate') ?>';
			});


			$(document).ready(function() {

				var urlParams = new URLSearchParams(window.location.search);
				var startDate = urlParams.get('startDate') ? urlParams.get('startDate') : new Date();
				var endDate = urlParams.get('endDate') ? urlParams.get('endDate') : new Date();
				console.log(startDate);
				console.log(endDate);

				$('#startDate').datepicker({
					dateFormat: 'yy-mm-dd',
					minDate: 0
				}).datepicker("setDate", startDate);

				$('#endDate').datepicker({
					dateFormat: 'yy-mm-dd',
					minDate: 1
				}).datepicker("setDate", endDate);

				$("#startDate").change(function(selected) {
					var date2 = $('#startDate').datepicker('getDate', '+1d');
					date2.setDate(date2.getDate() + 1);

					$("#endDate").datepicker({
						dateFormat: 'yy-mm-dd',
						minDate: 0
					}).datepicker("setDate", date2);
				})

			});
		</script>
</body>

</html>