<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/custom/ringkasan.css') ?>">

    <!-- GLOBAL CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/global/global.css') ?>">

    <!-- Rating -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

    <div class="logo" >
        <!-- <img src="<?php echo base_url() ?>assets/imgs/Logo-minor-lg.png" alt=""> -->
    </div>

    <div class="tanggal">
        <div class="row">
            <div class="col-1">
                <minor-md-title>Tanggal</minor-md-title>
            </div>
            <div class="col-5">
                <minor-md-title class="tanggal"><?= $date ?></minor-md-title>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-8">
            <div class="box">
                <div class="box-header">
                    <minor-lg-title>Ringkasan Pembayaran</minor-lg-title>
                </div>
                <div class="box-body">
                    <div class="total">
                        <minor-md-title>Total Penyewaan</minor-md-title>
                        <div class="row new">
                            <div class="col-8">
                                <minor-label-sm>- <?= $trx->merk ?> <span>(<?= $trx->transactionId ?>)</span></minor-label-sm>
                            </div>
                            <div class="col-4" style="text-align: right">
                                <minor-label-sm><span><?= rupiah($trx->totalAmount); ?></span></minor-label-sm>
                            </div>
                        </div>
                        
                    </div>
                    <div class="garis"></div>
                    <div class="total">
                        <div class="row">
                            <div class="col-8">
                                <minor-md-title>Subtotal</minor-md-title>
                            </div>
                            <div class="col-4" style="text-align: right">
                                <minor-label-sm><span><?= rupiah($trx->totalAmount); ?></span></minor-label-sm>
                            </div>
                        </div>
                    </div>
                    <div class="garis"></div>
                    <div class="total">
                        <minor-md-title>Catatan Penyewa</minor-md-title>
                        <div class="row new">
                            <div class="col-10 catatan">
                                <minor-label-sm>- Lokasi Pengambilan : <?= $trx->startPickupLocation ?></minor-label-sm></br>
                                <minor-label-sm>- Lokasi Pengembalian : <?= $trx->endPickupLocation ?></minor-label-sm></br>
                                <minor-label-sm>- Catatan : <?= $trx->notes ?></minor-label-sm>
                            </div>
                        </div>
                    </div>
                    <div class="garis"></div>
                    <div class="total">
                    <form action="<?php echo base_url().'rentalTransaction/uploadEvidenceTransfer'; ?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                            <input class="ket" type="hidden" name='transactionId' value="<?= $trx->transactionId?>" >
                            <minor-md-title>Upload Bukti Pembyaran</minor-md-title>
                            <div class="row new">
                                <div class="col-10 catatan">
                                    <div class="box-upload">
                                        <input type="file" name='evidenceTransfer' class="ngumpet-coy" onchange="readURL(this);">
                                        <img id="upload"  class="belom" src="<?php echo base_url() ?>assets/imgs/add.png">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 bukti">
                                <button type="submit" class="btn btn-outline-success">Kirim Bukti Transfer</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <minor-label-sm class="text-danger">Catatan : penyewaan bisa di batalkan dalam waktu 1x24 jam setelah pembayaran</minor-label-sm>
        </div>

        <div class="col-3 ml-auto">
            <div class="box-bank">
                <div class="box-header-bank">
                    <minor-medium> Rekening Minor</minor-medium>
                </div>
                <div class="box-header-bank-body">
                    <div class="list-minor-bank">
                        <div class="bca">
                            <div class="row">
                                <div class="col-12 list-bank">
                                    <img src="<?php echo base_url() ?>assets/imgs/bca.png">
                                    <minor-md-title>BANK BCA</minor-md-title>
                                </div>
                            </div>
                            <div class="col-12">
                                <minor-md-title>Atas Nama : Minor - 1029384576</minor-md-title>
                            </div>
                        </div>
                        <div class="spacer"></div>
                        <div class="bca">
                            <div class="row">
                                <div class="col-12 list-bank">
                                    <img src="<?php echo base_url() ?>assets/imgs/mandiri.png">
                                    <minor-md-title>BANK Mandiri</minor-md-title>
                                </div>
                            </div>
                            <div class="col-12">
                                <minor-md-title>Atas Nama : Minor - 1029384576</minor-md-title>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 btn-akhir">
            <a type="button" href='<?php echo base_url().'rentalTransaction/cancelTransaction/'.$trx->transactionId; ?>' class="btn btn-mnr-danger margin">Batalkan Penyewaan</a>
            <!-- <button type="button" class="btn btn-mnr-sukses">SELESAI</button> -->
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>

    <script>
        $("#ringkasan").click(function() {
            window.location.href = '<?php echo site_url('home/ringkasan') ?>';
        });
        
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#upload')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
</body>

</html>
