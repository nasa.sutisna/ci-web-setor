<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/custom/list-item.css') ?>">

    <!-- GLOBAL CSS -->

    <!-- Rating -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

    <form action="<?= site_url('home') ?>" method="GET">
        <input type="hidden" value="<?= $params['city'] ?>" name="city">
        <input type="hidden" value="<?= $params['startDate'] ?>" name="startDate">
        <input type="hidden" value="<?= $params['endDate'] ?>" name="endDate">
        <input type="hidden" value="<?= $params['type'] ?>" name="type">

        <div class="banner">
            <div class="header">
                <div class="logo">
                    <img src="<?= base_url() ?>assets/imgs/Log-minor-putih.png">
                </div>
                <div class="keterangan" style="text-transform:capitalize">
                    <minor-title><?= $params['city']; ?></minor-title>
                    <div class="sub">
                        <minor-label><?= dateFormat($params['startDate']); ?> - <?= dateFormat($params['endDate']); ?></minor-label>
                        <div class="garis"></div>
                        <minor-label><?= $params['type']; ?></minor-label>
                    </div>
                </div>
                <div class="ubah">
                    <div class="kotak">
                        <minor-medium><button class="btn-edit" type="submit">Ubah Pencarian</button></minor-medium>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="container">
        <div class="new-list data=">
            <div class="row ">
                <?php foreach ($items as $item) : ?>
                    <div class="col-md-3 col-sm-12 turunlah">
                        <div class="percard" data="<?= '?city=' . $this->input->get('city') . '&type=' . $this->input->get('type') . '&startDate=' . $this->input->get('startDate') . '&endDate=' . $this->input->get('endDate'); ?>" style="text-transform:capitalize" id="<?php echo $item['vendorId'] ?>">
                            <div class="card" style="width: 16rem;">
                                <img class="yangbenersih" src="<?php echo base_url("images/vendor/" . $item['vendorId'] . '/' . $item['picture1']) ?>" alt="Card image cap">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="title">
                                                <minor-md-title><?php echo $item['merk'] ?></minor-md-title>
                                            </div>
                                            <div class="new-lokasi d-flex">
                                                <div class="new-icon col-2">
                                                    <img src="<?php echo base_url() ?>assets/imgs/lokasi.svg" alt="">
                                                </div>
                                                <div class="new-desc col-10">
                                                    <minor-label-sm><?php echo $params['city'] ?></minor-label-sm>
                                                </div>
                                            </div>
                                            <div class="new-gigi d-flex">
                                                <div class="new-icon col-2">
                                                    <img src="<?php echo base_url() ?>assets/imgs/gear.svg" alt="">
                                                </div>
                                                <div class="new-desc col-10">
                                                    <minor-label-sm><?php echo $params['type'] ?></minor-label-sm>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-minor text-light">
                                    <minor-medium><?= rupiah($item['price']); ?></minor-medium>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script>
        $(".percard").click(function() {
            let id = $(this).attr("id");
            let date = $(this).attr("data");
            window.location.href = '<?php echo site_url('motor/detail/') ?>' + id + date;
        });
    </script>
</body>

</html>