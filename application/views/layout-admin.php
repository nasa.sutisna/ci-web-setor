<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

	<!-- Custom CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/custom/home.css') ?>">

	
	<!-- Custom CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/custom/dashboard.css') ?>">

	<!-- Global CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/global/global.css') ?>">

	<title>Sistem Sewa Motor</title>
</head>

<body>
	<div id="header">
		<div id="menu"><?php $this->load->view('includes/admin/menu'); ?></div>
	</div>

	<div id="content">
		<?php echo $contents ?>
	</div>

	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>

</body>

</html>
