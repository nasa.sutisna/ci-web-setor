<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Minor</title>
</head>


<body>

	<div class="d-flex" id="wrapper">

		<!-- Sidebar -->
		<div class="bg-light border-right" id="sidebar-wrapper">
			<div class="sidebar-heading">
				<img src="<?php echo base_url() ?>assets/imgs/Logo-minor.svg">
			</div>
			<div class="list-group list-group-flush" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				<a class="list-group-item list-group-item-action bg-light" id="v-pills-dashboard-tab"href="<?= site_url('rentalTransaction')?>" role="tab" aria-controls="v-pills-dashboard" aria-selected="true">Dashboard</a>
				<a class="list-group-item list-group-item-action bg-light" id="v-pills-motor-tab"  href="<?= site_url('Motor')?>" role="tab" aria-controls="v-pills-motor" aria-selected="false">Motor</a>
				<a class="list-group-item list-group-item-action bg-light" id="v-pills-motor-tab"  href="<?= site_url('Vendor')?>" role="tab" aria-controls="v-pills-vendor" aria-selected="false">Vendor</a>
				<a class="list-group-item list-group-item-action bg-light" id="v-pills-list-tab"  href="<?php echo site_url('login/logout') ?>" role="tab" aria-controls="v-pills-list" aria-selected="false">Keluar</a>
			</div>
		</div>
		<!-- /#sidebar-wrapper -->

		<!-- Page Content -->

		<!-- /#page-content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- Bootstrap core JavaScript -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>

	<!-- Menu Toggle Script -->
	<script>
		$("#menu-toggle").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("toggled");
		});
	</script>

</body>

</html>
