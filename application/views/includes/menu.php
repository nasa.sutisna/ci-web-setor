<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

	<!-- Custom CSS -->
	<link rel="stylesheet" href="<?php echo base_url('assets/custom/menu.css') ?>">

	<title>Minor</title>
</head>

<body>
	<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">
			<img src="<?php echo base_url() ?>assets/imgs/Logo-minor.svg">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbar-collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a class="nav-link js-scroll-trigger" href="<?php echo site_url('') ?>" data-target="navbar-collapse">Home</a>
				</li>
				<?php if (empty($this->session->userdata('email'))) : ?>
					<li class="nav-item">
						<a class="nav-link js-scroll-trigger" href="<?php echo site_url('member/register') ?>" data-target="navbar-collapse">Daftar</a>
					</li>
					<li class="nav-item">
						<a class="nav-link js-scroll-trigger" href="<?php echo site_url('member/login') ?>" data-target="navbar-collapse">Login</a>
					</li>
				<?php endif; ?>

				<?php if (!empty($this->session->userdata('email'))) : ?>
					<li class="nav-item" style="text-transform:capitalize">
						<a class="nav-link js-scroll-trigger" href="<?php echo site_url('home/listtrx') ?>" data-target="navbar-collapse"><?php echo $this->session->userdata('fullName'); ?></a>
					</li>
					<li class="nav-item">
						<a class="nav-link js-scroll-trigger" href="<?php echo site_url('login/logout') ?>" data-target="navbar-collapse">Keluar</a>
					</li>
				<?php endif; ?>

			</ul>
		</div>

	</nav>



	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>

	<!-- MY SCRIPT -->
	<script type="text/javascript">
		$('.js-scroll-trigger').click(function() {
			$('.navbar-collapse').collapse('show');
		});
	</script>

</body>

</html>
