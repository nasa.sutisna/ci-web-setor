<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/custom/emptystate.css') ?>">

    <!-- GLOBAL CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/global/global.css') ?>">

    <!-- Rating -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

        <div class="empty-state">
            <div class="myimg">
                <img src="<?php echo base_url() ?>assets/imgs/empty.png">
            </div>
            <div class="status">
                <minor-title>MAAF, Data Tidak dapat Di temukan coba ubah pencarian yuk</minor-title>
            </div>
        </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script>
        $("#ubah").click(function() {
            window.location.href = '<?php echo site_url('/') ?>';
        });

        $(".motor-item").click(function() {
            let id = $(this).attr("id");
            window.location.href = '<?php echo site_url('motor/detail/') ?>' + id;
        });
    </script>
</body>

</html>