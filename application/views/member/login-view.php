<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/custom/login.css') ?>">


    <title>Login</title>
</head>

<body>
    <div class="box">
        <div class="content">
            <div class="logo">
                <img src="<?php echo base_url() ?>assets/imgs/Logo-minor.svg" alt="">
                <h3>LOGIN</h3>
			</div>
			<?php echo isset($message) ? '<script>alert("'.$message.'")</script>': '';?>
            <form action="<?php echo site_url('login/postLogin?current='.$this->input->get('current'))?>" method="post" class="needs-validation" novalidate>
                <div class="username">
                    <p>Email</p>
                    <input type="text" placeholder="Masukkan email anda" name="email"> 
                </div>
                <div class="password">
                    <p>Password</p>
                    <input id="my-input" type="password" placeholder="Masukkan password" name="password">
                    <div class="mata" onclick="myFunction()">
                        <img src="<?php echo base_url() ?>assets/imgs/mata.svg" alt="">
                    </div>
                </div>

            <div class="footer">
                <button type="submit" class="my-btn text-center">LOGIN</button>
                <p>Belum memiliki account?<a href="<?php echo site_url('member/register') ?>">Daftar</a></p>
            </div>
			</form>
        </div>
    </div>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>

    <!-- My Script -->

    <script>
        function myFunction() {
            var x = document.getElementById("my-input");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }
    </script>
</body>

</html>
