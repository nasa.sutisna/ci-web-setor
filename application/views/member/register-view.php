<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/custom/regis.css') ?>">

    <!-- GLOBAL CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/global/global.css') ?>">


    <title>Registration</title>
</head>

<body>


    <div class="d-flex justify-content-center">
        <div class="box">
            <div class="header d-flex justify-content-center">
                <img src="<?php echo base_url() ?>assets/imgs/Logo-minor.svg" alt="">
                <minor-title>REGISTER</minor-title>
            </div>
            <?php echo isset($message) ? '<script>alert("'.$message.'")</script>': '';?>
            <div class="body">
               <form action="<?php echo base_url().'member/postRegister'; ?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                    <div class="form-section">
                        <p>Nama Lengkap</p>
                        <input type="text" placeholder="Masukan Nama Lengkap anda" name="name">
                    </div>
                    <div class="form-section">
                        <p>Email</p>
                        <input type="text" placeholder="Contoh@gmail.com" name="email">
                    </div>
                    <div class="form-section">
                        <p>Password</p>
                        <div class="pass">
                            <input id="passwordd" type="password" name="password" placeholder="Masukan Password">
                            <img onclick="pass()" src="<?php echo base_url() ?>assets/imgs/mata.svg" alt="">
                        </div>
                    </div>
                    <div class="form-section">
                        <p>Konfirmasi Password</p>
                        <div class="pass">
                            <input id="confirm" type="password" name="password_confirm" placeholder="Masukan Konfirmasi Password">
                            <img onclick="passconf()" src="<?php echo base_url() ?>assets/imgs/mata.svg" alt="">
                        </div>
                    </div>
                    <div class="form-section">
                        <p>No Telepon</p>
                        <input type="number" placeholder="Contoh 0812345678990" name="phone">
                    </div>
                    <div class="form-section">
                        <p>No Identitas</p>
                        <input type="number" placeholder="Masukan Nomor KTP anda" name="id">
                    </div>
                    <div class="gender">
                        <p>Jenis Kelamin</p>
                        <input type="radio" name="gender" value="male"> Laki-laki<br>
                        <input type="radio" name="gender" value="female"> Perempuan<br>
                    </div>
                    <div class="form-section">
                        <p>Alamat Lengkap</p>
                        <textarea name="address" class="form-control" rows="3"></textarea>
                    </div>
                    <div class="upload">
                        <p>Upload informasi diri</p>
                        <div class="row">
                            <div class="col-sm-12 col-md-4">
                                <div class="box-up">
                                    <img id="upktp" class="belom" src="<?php echo base_url() ?>assets/imgs/add-imag.png">
                                </div>
                                <label class="upload">UPLOAD KTP</label>
                                <input type="file" name="ktp" class="ngumpet-coy" onchange="takeKtp(this);">
                            </div>
                            <div class="col-sm-12 col-md-4 ">
                                <div class="box-up">
                                    <img id="upsim" class="belom" src="<?php echo base_url() ?>assets/imgs/add-imag.png">
                                </div>
                                <label class="upload">UPLOAD SIM</label>
                                <input type="file" name="sim" class="ngumpet-coy" onchange="takeSim(this);">
                            </div>
                            <div class="col-sm-12 col-md-4  ">
                                <div class="circle-up">
                                    <img id="upprof" class="belom" src="<?php echo base_url() ?>assets/imgs/add-imag.png">
                                </div>
                                <label class="upload">UPLOAD FOTO</label>
                                <input type="file" name="photo" class="ngumpet-coy" onchange="profil(this);">
                            </div>
                        </div>
                    </div>
                    <div class="user-agree">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
                            <label class="custom-control-label" for="customCheck">Saya setuju dengan <span>persyaratan dan ketentuan yang ada</span></label> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="my-btn text-center btn-block">DAFTAR</button>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-12 sign">
                            <minor-label-sm>Sudah Memiliki Account? <a href="<?php echo site_url('member/login') ?>">MASUK</a></minor-label-sm>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>

    <!-- My Script -->
    <script>
        function pass() {
            var x = document.getElementById("passwordd");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }

        function passconf() {
            var x = document.getElementById("confirm");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }

        function profil(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#upprof')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function takeSim(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#upsim')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function takeKtp(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#upktp')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>



</body>

</html>
