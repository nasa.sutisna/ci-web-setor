<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8'>
	<meta http-equiv='X-UA-Compatible' content='IE=edge'>
	<title>Motor List</title>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<link rel='stylesheet' type='text/css' media='screen' href='main.css'>
	<script src='main.js'></script>
</head>
<body>
	<div>
	<form action="<?php echo site_url('motor/search') ?>" method="GET">
	<table>
		<tr>
			<td>
				<select name="city" placholder="pilih kota">
					<option value="jakarta" <?php if($this->input->get('city') == 'jakarta') {echo 'selected';}?>>Jakarta</option>
					<option value="bandung" <?php if($this->input->get('city') == 'bandung') {echo 'selected';}?>>Bandung</option>
					<option value="surabaya" <?php if($this->input->get('city') == 'surabaya') {echo 'selected';}?>>Surabaya</option>
				</select>
			</td>
			<td>
				<select name="type" placholder="pilih type">
					<option value="manual" <?php if($this->input->get('type') == 'manual') {echo 'selected';}?>>Manual</option>
					<option value="matic" <?php if($this->input->get('type') == 'matic') {echo 'selected';}?>>Matic</option>
				</select>
			</td>
			<td>
				<input type="date" name="startDate" value="<?php echo $this->input->get('startDate')?>" placeholder="pilih tanggal">
			</td>
			<td>
				<input type="date" name="endDate" placeholder="pilih tanggal" value="<?php echo $this->input->get('endDate')?>">
			</td>
			<td><input type="submit" value="Cari"></td>
			<td></td>
		</tr>
	</table>
	</form>

	</div>
	<?php foreach ($items as $item): ?>
		<div style="padding:10px;width:500px;border:1px solid silver;margin-top:5px">
			<div>Merk : <?php echo $item['merk']; ?></div>
			<div>price : <?php echo $item['price']; ?></div>
		</div>
	<?php endforeach;?>
</body>
</html>

