<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_model extends CI_Model {

	var $table = 'member';
	public function __construct()
	{
		parent::__construct();
	}

	function inputMember($data, $table)
	{
        $this->db->insert($table, $data);
	}
	
	function checkMember($table, $where)
	{		
		return $this->db->get_where($table, $where);
	}

	function detail($id){
		return $this->db->get_where($this->table, ['memberId' => $id])->row();
	}

	function doUpdateMember($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
    }

	function uploadProfil($id)
	{
		$path = './images/member/'.$id;

		if (!is_dir($path)) {
			mkdir($path, 0777, TRUE);
		}

		$config['upload_path'] = $path;
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['overwrite'] = true;
		$config['max_size']  = '2048';
		$config['remove_space'] = TRUE;
	  
		$this->load->library('upload', $config);

		if($this->upload->do_upload('photo')){ 
		  $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
		  return $return;
		}else{
		  $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
		  return $return;
		}
	}

	function uploadSim($id)
	{
		$path = './images/member/'.$id;
		
		if (!is_dir($path)) {
			mkdir($path, 0777, TRUE);
		}

		$config['upload_path'] = $path;
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']  = '2048';
		$config['remove_space'] = TRUE;
	  
		$this->load->library('upload', $config);
		
		if($this->upload->do_upload('sim')){ 
		  $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
		  return $return;
		}else{
		  $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
		  return $return;
		}
	}

	function uploadKtp($id)
	{
		$path = './images/member/'.$id;

		if (!is_dir($path)) {
			mkdir($path, 0777, TRUE);
		}

		$config['upload_path'] = $path;
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']  = '2048';
		$config['remove_space'] = TRUE;
	  
		$this->load->library('upload', $config);

		if($this->upload->do_upload('ktp')){ 
		  $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
		  return $return;
		}else{
		  $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
		  return $return;
		}
	}
}
