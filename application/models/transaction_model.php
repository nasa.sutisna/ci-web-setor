<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_model extends CI_Model
{

    private $table = 'rental_transaction';

    public function __construct()
    {
        parent::__construct();
    }

    public function doCheckout($data, $table)
    {
        $this->db->insert($table, $data);
    }

    public function detailTransaction($where, $table)
    {
        // return $this->db->get_where($table, $where);
        $this->db->select('*');
        $this->db->from('rental_transaction a');
        $this->db->join('vendor b', 'b.vendorId = a.vendorId', 'left');
        $this->db->join('member m', 'm.memberId = a.memberId', 'left');
		$this->db->where('a.transactionId', $where);
		$this->db->order_by('transactionId','desc');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return $query;
        } else {
            return false;
        }
    }

	public function getMotorByVendor($id){
		return $this->db->get_where('motor',['vendorId' => $id])->result_array();;
	}

    public function listTransaction($id, $status)
    {
        $this->db->select('*');
        $this->db->from('rental_transaction a');
        $this->db->join('vendor b', 'b.vendorId=a.vendorId', 'left');
        $this->db->where('a.memberId', $id);
        $this->db->or_where_in('a.status', $status);
        $this->db->order_by('transactionId', 'desc');
        
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return $query;
        } else {
            return $query;
        }
    }

    public function listTrx($id, $status)
    {
        $this->db->select('a.*');
        $this->db->from('rental_transaction a');
        $this->db->join('vendor b', 'b.vendorId=a.vendorId', 'left');
        $this->db->or_where_in('a.status', $status);
        // $this->db->or_where('a.status', $status['status2']);
        $this->db->where('a.memberId', $id);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return $query;
        } else {
            return $query;
        }
    }

    public function doUpdateTransaction($where, $data, $table = 'rental_transaction')
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function doDeleteTransaction($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    public function uploadPict($transactionId)
    {
        $path = './images/EvidenceTransfer/' . $transactionId;

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['overwrite'] = true;
        $config['max_size'] = '2048';
        $config['remove_space'] = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('evidenceTransfer')) {
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        } else {
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

    public function listData($keyword, $limit, $start)
    {
        $this->db->select($this->table . '.*, vendor.merk');
        $this->db->from($this->table);
        $this->db->limit($limit, $start);
        $this->db->like($this->table . '.transactionId', $keyword);
        $this->db->or_like($this->table . '.status', $keyword);
        $this->db->or_like('vendor.merk', $keyword);
		$this->db->join('vendor', 'vendor.vendorId = ' . $this->table . '.vendorId');
		$this->db->order_by('transactionId','desc');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_count($keyword)
    {

        $this->db->select('count(*) as allcount');
        $this->db->from($this->table);

        if ($keyword != '') {
            $this->db->like($this->table . '.transactionId', $keyword);
            $this->db->or_like($this->table . '.status', $keyword);
            $this->db->or_like('vendor.merk', $keyword);
		}
		
		$this->db->join('vendor', 'vendor.vendorId = ' . $this->table . '.vendorId');
        $query = $this->db->get();
        $result = $query->result_array();

        return $result[0]['allcount'];
    }
}
