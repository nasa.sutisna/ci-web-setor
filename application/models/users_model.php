<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	function inputUser($data, $table){
        $this->db->insert($table, $data);
	}
	
	function checkUser($table, $where){		
		return $this->db->get_where($table, $where);
	}

	function hash($string)
    {
        return hash('sha256', $string . config_item('encryption_key'));
    }
	
	function getMemberInfo($email)
	{
		$this->db->select('*');
		$this->db->from('user a'); 
		$this->db->join('member b', 'b.email=a.email', 'left');
		$this->db->where('a.email',$email);     
		$query = $this->db->get(); 
		if($query->num_rows() != 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function doUpdateUser($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
    }
}
