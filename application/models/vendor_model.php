<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vendor_model extends CI_Model
{
    private $table_motor = 'motor';
    private $table_vendor = 'vendor';

    public function __construct()
    {
        parent::__construct();
    }

    public function doInputVendor($data, $table)
    {
       return $this->db->insert($table, $data);
    }

    public function detailVendor($where, $table = 'vendor')
    {
        $this->db->select('v.*,m.city');
        $this->db->from($this->table_vendor . ' v');
        $this->db->join($this->table_motor . ' m', 'm.vendorId = v.vendorId', 'left');
        $this->db->group_by('m.vendorId');
        return $this->db->get_where($table, ['v.vendorId' => $where['vendorId']]);
    }

    public function doUpdateVendor($where, $data, $table)
    {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    public function doDeleteVendor($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    public function uploadPict1($vendorId)
    {
        $path = './images/vendor/' . $vendorId;

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['overwrite'] = true;
        $config['max_size'] = '2048';
        $config['remove_space'] = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('pict1') == null) {
            $return = null;
            return $return;
        } else {
            if ($this->upload->do_upload('pict1')) {
                $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
                return $return;
            } else {
                $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
                return $return;
            }
        }
    }

    public function uploadPict2()
    {
        if ($this->upload->do_upload('pict2') == null) {
            $return = null;
            return $return;
        } else {
            if ($this->upload->do_upload('pict2')) {
                $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
                return $return;
            } else {
                $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
                return $return;
            }
        }
    }

    public function uploadPict3()
    {
        if ($this->upload->do_upload('pict3') == null) {
            $return = null;
            return $return;
        } else {
            if ($this->upload->do_upload('pict3')) {
                $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
                return $return;
            } else {
                $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
                return $return;
            }
        }
    }

    public function uploadPict4()
    {
        if ($this->upload->do_upload('pict4') == null) {
            $return = null;
            return $return;
        } else {
            if ($this->upload->do_upload('pict4')) {
                $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
                return $return;
            } else {
                $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
                return $return;
            }
        }
    }

    public function getVendorId()
    {
        $this->db->select('vendorId, merk');
        $this->db->from('vendor');
        $query = $this->db->get();

        return $query;
    }

    public function get_count($keyword)
    {

        $this->db->select('count(*) as allcount');
        $this->db->from($this->table_vendor);

        if ($keyword != '') {
            $this->db->like($this->table_vendor . '.vendorId', $keyword);
            $this->db->or_like($this->table_vendor . '.type', $keyword);
            $this->db->or_like('merk', $keyword);
		}
		
        $query = $this->db->get();
        $result = $query->result_array();

        return $result[0]['allcount'];
    }

    public function listData($keyword, $limit, $start)
    {
        $this->db->select('*');
        $this->db->from('vendor');
        $this->db->limit($limit, $start);
        $this->db->like('vendorId', $keyword);
        $this->db->or_like('type', $keyword);
        $this->db->or_like('merk', $keyword);
        $this->db->order_by('vendorId', 'asc');
        $query = $this->db->get();

        return $query->result();
    }
}
