<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Motor_model extends CI_Model
{
    private $table_motor = 'motor';
    private $table_vendor = 'vendor';

    public function __construct()
    {
        parent::__construct();
    }

    public function getList($city = '', $type = '')
    {
        $this->db->select('v.*,m.city');
        $this->db->from($this->table_vendor . ' v');
        $this->db->like('m.city', $city);
        $this->db->like('v.type', $type);
        $this->db->where('m.status', 'ready');
		$this->db->join($this->table_motor . ' m', 'm.vendorId = v.vendorId');
		$this->db->group_by('m.vendorId');
        $query = $this->db->get();
        return $query;
    }

    function doInputMotor($data, $table)
	{
        $this->db->insert($table, $data);
	}

    function detailMotor($where, $table = 'motor'){		
		$this->db->select('motor.*,vendor.*');
		$this->db->join('vendor','vendor.vendorId = motor.vendorId');
		return $this->db->get_where($table, $where);
    }

    function doUpdateMotor($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
    }
    
    function doDeleteMotor($where, $table){
        $this->db->where($where);
        $this->db->delete($table);
	}
	
	function getMotorByVendor($vendorId, $status = 'ready'){
		return $this->db->get_where($this->table_motor, ['vendorId' => $vendorId, 'status' => $status])->result_array();
    }
    
    public function get_count($keyword)
    {

        $this->db->select('count(*) as allcount');
        $this->db->from($this->table_motor);

        if ($keyword != '') {
            $this->db->like($this->table_motor . '.motorId', $keyword);
            $this->db->or_like($this->table_motor . '.status', $keyword);
            $this->db->or_like('vendor.merk', $keyword);
		}
		
		$this->db->join('vendor', 'vendor.vendorId = ' . $this->table_motor . '.vendorId');
        $query = $this->db->get();
        $result = $query->result_array();

        return $result[0]['allcount'];
    }

    public function listData($keyword, $limit, $start)
    {
        $this->db->select('motor.*, vendor.merk');
        $this->db->from('motor');
        $this->db->limit($limit, $start);
        $this->db->like('motor.motorId', $keyword);
        $this->db->or_like('motor.status', $keyword);
        $this->db->or_like('vendor.merk', $keyword);
        $this->db->join('vendor', 'vendor.vendorId = motor.vendorId');
        $query = $this->db->get();

        return $query->result();
    }
}
