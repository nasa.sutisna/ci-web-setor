<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Motor extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("motor_model");
        $this->load->model("vendor_model");
        $this->load->model("member_model");
        $this->load->library("pagination");
    }

    public function index()
    {
        middlewareAdmin();
		$config = array();
		$keyword = $this->input->get('q');
		
        $config["base_url"] = base_url() . "Motor";
        $config["total_rows"] = $this->motor_model->get_count($keyword);
        $config["per_page"] = 5;
        $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$skip = ($page > 1 ) ? ($page - 1) * $config['per_page'] : 0;
        $data["links"] = $this->pagination->create_links();
        $data['contents'] = $this->motor_model->listData($keyword, $config["per_page"], $skip);
        $data['message'] = $this->session->flashdata('message');
		
        $this->template_admin->view('admin/list-motor', $data);
    }

    public function getInputMotor()
    {
        $data['items'] = $this->vendor_model->getVendorId()->result_array();
        $this->template_admin->view('admin/input-motor', $data);
    }

    public function getUpdateMotor($id)
    {
        $where = array('motorId' => $id);
        $data['motor'] = $this->motor_model->detailMotor($where)->row();
        $data['items'] = $this->vendor_model->getVendorId()->result_array();

        $this->template_admin->view('admin/update-motor', $data);
    }

    public function search()
    {
        $city = $this->input->get('city');
        $type = $this->input->get('type');
        $startDate = $this->input->get('startDate');
        $endDate = $this->input->get('endDate');
		
		$data['params'] = Array('city' => $city, 'type' => $type, 'startDate' => $startDate, 'endDate' => $endDate);
		$data['items'] = $this->motor_model->getList($city, $type);
		$this->template->view('home/list-item',$data);
    }

    public function inputMotor()
    {
        //set variable from request message
        $motorId = $this->input->post('motorId');
        $vendorId = $this->input->post('vendorId');
        $city = $this->input->post('city');
        $colour = $this->input->post('colour');
        $status = $this->input->post('status');

        //set variable to table motor
        $motor = array(
            'motorId' => $motorId,
            'vendorId' => $vendorId,
            'city' => $city,
            'colour' => $colour,
            'status' => $status
        );

        //set condition
        $where = array('motorId' => $motorId);

        //check motorId
        $chek = $this->motor_model->detailMotor($where,'motor')->num_rows();

        //validation motorId is exist
        if($chek == 0){
            //if motor id not exist
            //insert to table motor
            $this->motor_model->doInputMotor($motor,'motor');

            //update stock vendor
            //set condition
            $where = array('vendorId' => $vendorId);
            $getStock = $this->vendor_model->detailVendor($where)->row();
            $update = $getStock->stock + 1;
            $stock = array( 'stock' => $update);

            $this->vendor_model->doUpdateVendor($where, $stock, 'vendor');

            $message = "Data Motor berhasil ditambahkan";
            $this->session->set_flashdata('message', $message);
            redirect('motor/index');
        }
        else{
            //if motor id exist
            $message = "ID Motor sudah terdaftar";
            $this->session->set_flashdata('message', $message);
            redirect('motor/index');
            
        }
    }

    public function getDetailMotor($id)
    {
        $where = array('motorId' => $id);
        $data['motor'] = $this->motor_model->detailMotor($where,'motor')->result();
        print_r($data['motor']);
    }

    public function updateMotor()
    {
        //set variable from request message
        $motorId = $this->input->post('motorId');
        $vendorId = $this->input->post('vendorId');
        $city = $this->input->post('city');
        $colour = $this->input->post('colour');
        $status = $this->input->post('status');

        //set condition
        $where = array('motorId' => $motorId);

        //set variable to table motor
        $motor = array(
            'vendorId' => $vendorId,
            'city' => $city,
            'colour' => $colour,
            'status' => $status
        );

        //check motorId
        $chek = $this->motor_model->detailMotor($where,'motor')->num_rows();

        //validation motorId is exist
        if($chek > 0){
            //if motorId exist
            //update table motor
            $this->motor_model->doUpdateMotor($where, $motor,'motor');

            $message = "Data Motor berhasil diupdate";
            $this->session->set_flashdata('message', $message);
            redirect('motor/index');
        }
        else{
            //if motorId not exist
            $message = "ID Motor tidak ditemukan";
            $this->session->set_flashdata('message', $message);
            redirect('motor/index');
        } 
    }

    function deleteMotor($mId, $vId){
        $where = array('motorId' => $mId);

        //check motorId
        $chek = $this->motor_model->detailMotor($where,'motor')->num_rows();

        //validation motorId is exist
        if($chek > 0){
            //if motorId exist
            $this->motor_model->doDeleteMotor($where, 'motor');

            //update stock vendor
            //set condition
            $where = array('vendorId' => $vId);
            $getStock = $this->vendor_model->detailVendor($where)->row();
            $update = $getStock->stock - 1;
            $stock = array( 'stock' => $update);

            $this->vendor_model->doUpdateVendor($where, $stock, 'vendor');

            $message = "Data Motor berhasil dihapus";
            $this->session->set_flashdata('message', $message);
            redirect('motor/index');
        }
        else{
            //if motorId not exist
            $message = "ID Motor tidak ditemukan";
            $this->session->set_flashdata('message', $message);
            redirect('motor/index');
        } 
	}
	
	public function detail($id){
		$where = array('vendorId' => $id);
		$detail = $this->vendor_model->detailVendor($where)->row();
		$relatedMotor = $this->motor_model->getMotorByVendor($id,'ready');
		
		$data['relatedMotor'] = $relatedMotor;
		$data['item'] = $detail;
		$data['listVendor'] = $this->motor_model->getList($this->input->get('city'), $this->input->get('type'));

		$this->template->view('home/detail-item',$data);
	}

	public function sewa($id){
		/** redirect ke login jika belum login */
		if(empty($this->session->userdata('memberId'))){
			/** supaya ketika login berhasil lansung redirect ke halaman sewa */
			$linkSewa = 'motor/sewa/'.$id.'?startDate='.$this->input->get('startDate').'&endDate='.$this->input->get('endDate').'&quantity='.$this->input->get('quantity');
			redirect('member/login?current='.base64_encode($linkSewa));
        }
        
        $start = new DateTime($this->input->get('startDate'));
        $end = new DateTime($this->input->get('endDate'));
        $interval = $start->diff($end);
        $duration = $interval->d;
        $totalSewa = $this->input->get('quantity');
       
		$where = array('vendorId' => $id);
		$detail = $this->vendor_model->detailVendor($where)->row();
		$memberId = $this->session->userdata('memberId');
		$detailMember = $this->member_model->detail($memberId);
	
		$data['item'] = $detail;
        $data['member'] = $detailMember;
        $data['duration'] = $duration;
        $data['totalSewa'] = $totalSewa;

		$this->template->view('home/sewa',$data);
	}
}
