<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("transaction_model");
	}

	public function index()
	{
		$this->template->view('home/index');
	}

	public function list(){
		$this->template->view('home/list-item');
	}

	public function detail(){
		$this->template->view('home/detail-item');
	}
	
	public function sewa(){
		$this->template->view('home/sewa');
	}
	
	public function emptystate(){
		$this->load->view('empty-state/empty');
	}
	public function ringkasan(){
		$this->load->view('home/ringkasan');
	}
	public function listtrx(){

		$memberId = $this->session->userdata('memberId');

		//set condition trx not payment
		$status1 = array('ORDER');
		$trx['notpay'] = $this->transaction_model->listTransaction($memberId, $status1)->result_array();

		//set condition trx payment
		$status2 = array('PAYMENT', 'RENT', 'APPROVE');
		$trx['pay'] = $this->transaction_model->listTransaction($memberId, $status2)->result_array();

		//set condition trx finish
		$status3 = array('FINISH', 'REJECT');
		$trx['finish'] = $this->transaction_model->listTransaction($memberId, $status3)->result_array();
		
		$this->template->view('home/list-trx', $trx);
	}


	/** ADMINISTRATOR HOME */

	public function adminHome(){
		$this->load->view('includes/admin/menu');
	}

	public function dashboard(){
		$this->template_admin->view('admin/dashboard');
	}

	public function listMotor(){
		$this->template_admin->view('admin/list-motor');
	}
	
	public function admtrx(){
		$this->template_admin->view('admin/detail-trx');
	}

}
