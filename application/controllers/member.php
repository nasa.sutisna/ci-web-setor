<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __construct()
	{
		// construct
		parent::__construct();
		$this->load->model("member_model");
		$this->load->model("users_model");
	}

	public function register()
	{
		$this->load->view('member/register-view');
	}

	public function login()
	{
		$this->load->view('member/login-view');
	}

	public function profile($id){
		$member = $this->member_model->detail($id);
		echo '<pre>';print_r($member);exit;
	}

	public function postRegister()
	{
		//set variable email from request message
		$email = $this->input->post('email');

		//set condition
		$check =  array(
			'email' => $email
		);

		//cek email in table member and user
		$checkMember = $this->member_model->checkMember('member', $check)->num_rows();
		$checkUser = $this->users_model->checkUser('user', $check)->num_rows();

		//validation email
		if($checkMember == 0 && $checkUser == 0)
		{
			//if email not exist
			//set variable from request message
			$fullName = $this->input->post('name');
			$gender = $this->input->post('gender');
			$address = $this->input->post('address');
			$phoneNumber = $this->input->post('phone');
			$id = $this->input->post('id');
			$photo = $_FILES["photo"]["name"];
			$password = $this->input->post('password');
			$password_confirm = $this->input->post('password_confirm');
			$ktp = $_FILES["ktp"]["name"];
			$sim = $_FILES["sim"]["name"];

			//validation password
			if(strcmp($password_confirm, $password) != 0)
			{
				//if password not match
				$data['message'] = "Password tidak sesuai dengan Konfirmasi Password ";
				$this->load->view('member/register-view',$data);	
			}
			else{
				//set variable to table member
				$member = array(
					'memberId' => $id,
					'fullName' => $fullName,
					'gender' => $gender,
					'address' => $address,
					'email' => $email,
					'phoneNumber' => $phoneNumber,
					'photo' => $photo,
					'ktp' => $ktp,
					'sim' => $sim
				);

				//set variable to  table user
				$user = array(
					'email' => $email,
					'password' => $this->users_model->hash($password),
					'role' => 0
				);

				//insert to table member
				$this->member_model->inputMember($member,'member');

				//insert to table user
				$this->users_model->inputUser($user,'user');

				//upload image
				$profil = $this->member_model->uploadProfil($id);
				$uploadSim = $this->member_model->uploadSim($id);
				$uploadKtp = $this->member_model->uploadKtp($id);

				$data['message'] = "Register Sukses";
				// $this->load->view('member/register-view',$data);
				$this->load->view('member/login-view', $data);

			}
		}
		else{
			//if email is exist
			$data['message'] = "email sudah terdaftar";
			$this->load->view('member/register-view',$data);
		}
	}

	public function updateMember()
	{
		//set variable email from request message
		$email = $this->input->post('email');

		//set condition
		$check =  array(
			'email' => $email
		);

		//cek email in table member
		$checkMember = $this->member_model->checkMember('member', $check)->num_rows();
		// $checkUser = $this->users_model->checkUser('user', $check)->num_rows();

		//validation email
		if($checkMember > 0)
		{
			//if email  exist
			//set variable from request message
			$id = $this->input->post('id');
			$fullName = $this->input->post('name');
			$address = $this->input->post('address');
			$phoneNumber = $this->input->post('phone');
			

			//set variable to table member
			$member = array(
				'memberId' => $id,
				'fullName' => $fullName,
				'address' => $address,
				'email' => $email,
				'phoneNumber' => $phoneNumber
			);

			//update to table member
			$this->member_model->doUpdateMember($check, $member, 'member');

			$data['message'] = "Update Profil Sukses, silahkan login kembali";
			$this->load->view('member/login-view', $data);
		}
		else{
			//if email is not exist
			$data['message'] = "Email tidak terdaftar";
			$this->template->view('home/index', $data);
		}
	}

	public function updatePassword()
	{
		//set variable email from request message
		$email = $this->input->post('email');

		//set condition
		$check =  array(
			'email' => $email
		);

		//cek email in table user
		$checkUser = $this->users_model->checkUser('user', $check)->num_rows();

		//validation email
		if($checkUser > 0)
		{
			//if email  exist
			//set variable from request message
			$password = $this->input->post('password');
			$password_confirm = $this->input->post('password_confirm');

			//validation password
			if(strcmp($password_confirm, $password) != 0)
			{
				//if password not match
				$data['message'] = "Password tidak sesuai dengan Konfirmasi Password ";
				$this->template->view('home/index', $data);
			} else {
				//set variable to  table user
				$user = array(
					'password' => $this->users_model->hash($password)
				);

				//update to table member
				$this->users_model->doUpdateUser($check, $user, 'user');

				$data['message'] = "Update Password Sukses, silahkan login kembali";
				$this->load->view('member/login-view', $data);
			}
		}
		else{
			//if email is not exist
			$data['message'] = "Email tidak terdaftar";
			$this->template->view('home/index', $data);
		}
	}
}


