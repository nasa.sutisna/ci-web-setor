<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RentalTransaction extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        checkAuth();
        $this->load->model("transaction_model");
        $this->load->model("vendor_model");
        $this->load->library("pagination");
        $this->load->helper('date');
    }

    public function index()
    {
        middlewareAdmin();
        $config = array();
        $keyword = $this->input->get('q');

        $config["base_url"] = base_url() . "RentalTransaction";
        $config["total_rows"] = $this->transaction_model->get_count($keyword);
        $config["per_page"] = 5;
        $config["uri_segment"] = 2;
        $config['use_page_numbers'] = true;
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$skip = ($page > 1 ) ? ($page - 1) * $config['per_page'] : 0;

        $data["links"] = $this->pagination->create_links();
        $data['contents'] = $this->transaction_model->listData($keyword, $config["per_page"], $skip);
        $this->template_admin->view('admin/list-transaction', $data);
    }

    public function checkout()
    {
        $status = 200;

        $format = "%Y%m%d%h%i%s";

        //set variable from request message
        $trnId = 'mnr-' . mdate($format);
        $memberId = $this->input->post('memberId');
        $vendorId = $this->input->post('vendorId');
        $rentalDate = $this->input->post('rentalDate');
        $returnDate = $this->input->post('returnDate');
        $duration = $this->input->post('duration');
        $startPickup = $this->input->post('startPickup');
        $endPickup = $this->input->post('endPickup');
        $totalAmount = $this->input->post('total');
        $itemTotal = $this->input->post('itemTotal');
        $notes = $this->input->post('notes');

        //set variable to table motor
        $checkout = array(
            'transactionId' => $trnId,
            'memberId' => $memberId,
            'vendorId' => $vendorId,
            'rentalDate' => $rentalDate,
            'returnDate' => $returnDate,
            'duration' => $duration,
            'startPickupLocation' => $startPickup,
            'endPickupLocation' => $endPickup,
            'totalAmount' => $totalAmount,
            'itemTotal' => $itemTotal,
            'status' => 'ORDER',
            'notes' => $notes,
        );

        //set condition
        $where = array('vendorId' => $vendorId);

        //check stock vendor
        $chek = $this->vendor_model->detailVendor($where, 'vendor')->result();

        foreach ($chek as $row) {

            //validation stock vendor
            if ($row->stock == 0) {
                //stock vendor 0
                echo "Maaf stock motor kosong";
            } else {
                //stock vendor not 0
                //insert transaction to table rental_transaction
                if ($startPickup == null) {
                    $status = 422;
                    $data['message'] = "Lokasi pengambilan dan pengembalian tidak boleh kosong";
                } else if ($endPickup == null) {
                    $status = 422;
                    $data['message'] = "Lokasi pengambilan dan pengembalian tidak boleh kosong";
                } else {
                    $this->transaction_model->doCheckout($checkout, 'rental_transaction');
                    $data['message'] = "success";
                    $data['transactionId'] = $trnId;
                }
            }
        }

        $this->output->set_status_header($status)
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function getDetailTransaction($id)
    {
        $getDetailTrx = $this->transaction_model->detailTransaction($id, 'rental_transaction')->row();
        $vendorId = $getDetailTrx->vendorId;

        $data['transaction'] = $getDetailTrx;
        $data['listMotor'] = $this->transaction_model->getMotorByVendor($vendorId);
        $this->template_admin->view('admin/detail-trx', $data);
    }

    public function getUploadTransfer($id)
    {
        // $where = array('transactionId' => $id);
        $format = "%d-%m-%Y";
        $data['date'] = mdate($format);
        $data['trx'] = $this->transaction_model->detailTransaction($id, 'rental_transaction')->row();

        $this->template->view('home/ringkasan', $data);
    }

    public function uploadEvidenceTransfer()
    {
        //set variable from request message
        $transactionId = $this->input->post('transactionId');
        $evidenceTransfer = $_FILES["evidenceTransfer"]["name"];

        //set condition
        $where = array('transactionId' => $transactionId);

        //set variable to table rental_transaction
        $transaction = array(
            'proofOfPayment' => $evidenceTransfer,
            'status' => 'PAYMENT',
        );

        //update table payment
        $paymentContent = array('transactionId' => $transactionId, 'transferEvidence' => $evidenceTransfer);

        //check transactionId
        $chek = $this->transaction_model->detailTransaction($transactionId, 'rental_transaction')->num_rows();

        //validation transactionId is exist
        if ($chek > 0) {
            //if transactionId exist
            //update table rental_transaction
            if ($evidenceTransfer == null) {
                $data['message'] = "Bukti Transfer tidak boleh kosong";
                $this->template->view('home/index', $data);
            } else {
                $this->transaction_model->doUpdateTransaction($where, $transaction, 'rental_transaction');
                $this->transaction_model->doCheckout($paymentContent, 'payment');
                $this->transaction_model->uploadPict($transactionId);

                $data['message'] = "Upload Bukti Transfer berhasil";
                $this->template->view('home/index', $data);
            }
        } else {
            //if transactionId not exist
            $data['message'] = "ID Transaksi tidak ditemukan";
            $this->template->view('home/index', $data);
        }
    }

    public function verifyTransaction()
    {
        //set variable from request message
        $transactionId = $this->input->post('transactionId');
        $motorId = $this->input->post('motorId');
        $status = $this->input->post('status');

        //set condition
        $where = array('transactionId' => $transactionId);

        //set variable to table rental_transaction
        $transaction = array(
            'motorId' => $motorId,
            'status' => $status,
        );

        //check transactionId
        $chek = $this->transaction_model->detailTransaction($where, 'rental_transaction')->num_rows();

        //validation transactionId is exist
        if ($chek > 0) {
            //if transactionId exist
            //update table rental_transaction
            $this->transaction_model->doUpdateTransaction($where, $transaction, 'rental_transaction');

            echo "Verify success";
        } else {
            //if transactionId not exist
            echo "transactionId not found";
        }
    }

    public function updateTransaction()
    {
        //set variable from request message
        $transactionId = $this->input->post('transactionId');
        $status = $this->input->post('status');

        //set condition
        $where = array('transactionId' => $transactionId);

        //set variable to table rental_transaction
        $transaction = array('status' => $status);

        //check transactionId
        $chek = $this->transaction_model->detailTransaction($where, 'rental_transaction')->num_rows();

        //validation transactionId is exist
        if ($chek > 0) {
            //if transactionId exist
            //update table rental_transaction
            $this->transaction_model->doUpdateTransaction($where, $transaction, 'rental_transaction');

            echo "Update transaction success";
        } else {
            //if transactionId not exist
            echo "transactionId not found";
        }
    }

    public function detail()
    {
        $this->template->view('home/ringkasan');
    }

    public function cancelTransaction($id)
    {
        // //set variable from request message
        // $transactionId = $this->input->post('transactionId');
        // $status = $this->input->post('status');

        //set condition
        $where = array('transactionId' => $id);

        //check transactionId
        $chek = $this->transaction_model->detailTransaction($id, 'rental_transaction')->num_rows();

        //validation transactionId is exist
        if ($chek > 0) {
            //if transactionId exist
            //update table rental_transaction
            $this->transaction_model->doDeleteTransaction($where, 'rental_transaction');

            $data['message'] = "Transaksi berhasil dibatalkan";
            $this->template->view('home/index', $data);
        } else {
            //if transactionId not exist
            $data['message'] = "ID Transaksi tidak ada";
            $this->template->view('home/index', $data);
        }
    }

    public function approval()
    {
        $idTrx = $this->input->post('transactionId');
		$motorList = $this->input->post('motorList');
		$note = $this->input->post('note');
		$message = '';

		if(empty($motorList) && $note == ''){
			$message = 'Pilih motor yang akan di sewakan !';
			$data['message'] = $message;
			$getDetailTrx = $this->transaction_model->detailTransaction($idTrx, 'rental_transaction')->row();
			$vendorId = $getDetailTrx->vendorId;
	
			$data['transaction'] = $getDetailTrx;
			$data['listMotor'] = $this->transaction_model->getMotorByVendor($vendorId);
			$this->template_admin->view('admin/detail-trx',$data);
		}
		else{
			$content['motorId'] = json_encode($motorList);
			$content['notes'] = $note;
			$content['status'] = $note == '' ? 'APPROVE' : 'REJECT';
			$this->transaction_model->doUpdateTransaction(['transactionId' => $idTrx], $content);
			$this->session->set_flashdata('item',['message' => 'berhasil disimpan']);
			redirect('rentalTransaction');
		}
    }

    public function listTransaction()
    {
        $this->template_admin->view('admin/list-transaction');
    }

    public function detailTrx()
    {
        $this->template_admin->view('admin/detail-trx');
    }
}
