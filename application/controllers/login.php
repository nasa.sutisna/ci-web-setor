<?php 
 
class Login extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		$this->load->model("users_model");
	}
 
	function postLogin(){
		$email = $this->input->post('email');
		$password = $this->users_model->hash($this->input->post('password'));
		$setEmail = array('email' => $email);

		$cek = $this->users_model->checkUser("user", $setEmail)->num_rows();

		//validation email is exist
		if($cek == 0)
		{
			//email not exist
			$data['message'] = "email belum terdaftar";
			$this->load->view('member/login-view',$data);
		}
		else{
			//email is exist
			$where = array(
				'email' => $email,
				'password' => $password
				);
			
			$auth = $this->users_model->checkUser("user", $where)->num_rows();

			//authentication email and password
			if($auth > 0){
				//if auth true
				//get data member
				$memberInfo = $this->users_model->getMemberInfo($email);
	
				foreach ($memberInfo as $row){
					$data_session = array(
								'memberId'      => $row->memberId,
								'fullName'      => $row->fullName,
								'gender'        => $row->gender,
								'address'       => $row->address,
								'phoneNumber'   => $row->phoneNumber,
								'email'  		=> $row->email,
								'role'			=> $row->role,
								'photo'			=> $row->photo,
								'ktp'			=> $row->ktp,
								'sim'			=> $row->sim,
								'login'     	=> TRUE
								);
				}
	 
				//set data member to session
				$this->session->set_userdata($data_session);
	
				//validation role
				if($this->session->userdata('role') == 1){
					redirect('rentalTransaction');
				}
				else{
	
					$data = $this->session->userdata('email');
					$currentUri = $this->input->get('current');
					$uri = '';
					if($currentUri){
						$uri = base64_decode($currentUri);
						redirect($uri);
					}
					else{
						redirect('/');
					}					
				}
	
			}else{
				//auth false
				$data['message'] = "password tidak cocok";
				$this->load->view('member/login-view',$data);
			}
		}
	}
 
	function logout(){
		$this->session->sess_destroy();
		redirect('/');
	}
}
