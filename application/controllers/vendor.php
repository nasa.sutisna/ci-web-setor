<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {

	public function __construct()
	{
		// construct
		parent::__construct();
        $this->load->model("vendor_model");
        $this->load->helper('date');
        $this->load->library("pagination");
    }

    public function index()
    {
        middlewareAdmin();
		$config = array();
		$keyword = $this->input->get('q');
		
        $config["base_url"] = base_url() . "Vendor";
        $config["total_rows"] = $this->vendor_model->get_count($keyword);
        $config["per_page"] = 5;
        $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$skip = ($page > 1 ) ? ($page - 1) * $config['per_page'] : 0;
		
        $data["links"] = $this->pagination->create_links();
        $data['contents'] = $this->vendor_model->listData($keyword, $config["per_page"], $skip);
        $data['message'] = $this->session->flashdata('message');
		
        $this->template_admin->view('admin/list-vendor', $data);
    }
    
    public function getInputVendor()
    {
        $this->template_admin->view('admin/input-vendor');
    }

    public function getUpdateVendor($id)
    {
        $where = array('vendorId' => $id);
        $data['items'] = $this->vendor_model->detailVendor($where)->row();

        $this->template_admin->view('admin/update-vendor', $data);
    }

    public function inputVendor()
    {
        $format = "%Y%m%d%h%i%s";
        $vendorId = mdate($format);

        //set variable from request message
        $merk = $this->input->post('merk');
        $type = $this->input->post('type');
        $price = $this->input->post('price');
        $OverDueFee = $this->input->post('overDueFee');

        //set variable to table vendor
        $vendor = array(
            'vendorId' => $vendorId,
            'merk' => $merk,
            'type' => $type,
            'price' => $price,
            'OverDueFee' => $OverDueFee,
            'stock' => 0,
            'picture1' => $_FILES["pict1"]["name"],
            'picture2' => $_FILES["pict2"]["name"],
            'picture3' => $_FILES["pict3"]["name"],
            'picture4' => $_FILES["pict4"]["name"]
        );

        //insert to table vendor
        $save = $this->vendor_model->doInputVendor($vendor,'vendor');

        if($save == 1)
        {
            //upload file
            $picture1 = $this->vendor_model->uploadPict1($vendorId);
            $picture2 = $this->vendor_model->uploadPict2();
            $picture3 = $this->vendor_model->uploadPict3();
            $picture4 = $this->vendor_model->uploadPict4();

            $message = "Data Vendor berhasil ditambahkan";
            $this->session->set_flashdata('message', $message);
            redirect('vendor/index');
        } else{
            $message = "Data Vendor gagal ditambahkan";
            $this->session->set_flashdata('message', $message);
            redirect('vendor/index');
        }
    }

    public function getDetailVendor($id)
    {
        $where = array('vendorId' => $id);
        $data['vendor'] = $this->vendor_model->detailVendor($where,'vendor')->result();
        print_r($data['vendor']);
    }

    public function updateVendor()
    {
        //set variable from request message
        $vendorId = $this->input->post('vendorId');
        $merk = $this->input->post('merk');
        $type = $this->input->post('type');
        $price = $this->input->post('price');
        $OverDueFee = $this->input->post('overDueFee');
        $stock = $this->input->post('stock');
        $picture1 = $_FILES["pict1"]["name"];
        $picture2 = $_FILES["pict2"]["name"];
        $picture3 = $_FILES["pict3"]["name"];
        $picture4 = $_FILES["pict4"]["name"];

        //set conditional
        $where = array('vendorId' => $vendorId);

        //check vendorId
        $chek = $this->vendor_model->detailVendor($where,'vendor');

        //validation vendorId is exist
        if($chek->num_rows() > 0){
            //if vendorId exist
            //get picture existing
            foreach ($chek->result() as $row){

                if($picture1 == null){
                    $picture1 = $row->picture1;
                }

                if($picture2 == null){
                    $picture2 = $row->picture2;
                }

                if($picture3 == null){
                    $picture3 = $row->picture3;
                }

                if($picture4 == null){
                    $picture4 = $row->picture4;
                }
            }
            
            //set variable to table vendor
            $vendor = array(
                'merk' => $merk,
                'type' => $type,
                'price' => $price,
                'OverDueFee' => $OverDueFee,
                'stock' => $stock,
                'picture1' =>$picture1,
                'picture2' => $picture2,
                'picture3' => $picture3,
                'picture4' => $picture4
            );

            //update table vendor
            $update = $this->vendor_model->doUpdateVendor($where, $vendor,'vendor');

            if($update == 1)
            {
                //upload picture
                $picture1 = $this->vendor_model->uploadPict1($vendorId);
                $picture2 = $this->vendor_model->uploadPict2();
                $picture3 = $this->vendor_model->uploadPict3();
                $picture4 = $this->vendor_model->uploadPict4();

                $message = "Data Vendor berhasil diupdate";
                $this->session->set_flashdata('message', $message);
                redirect('vendor/index');
            } else{
                $message = "Data Vendor gagal diupdate";
                $this->session->set_flashdata('message', $message);
                redirect('vendor/index');
            }          
        }
        else{
            //if vendorId not exist
            $message = "ID Vendor tidak ditemukan";
            $this->session->set_flashdata('message', $message);
            redirect('vendor/index');
        } 
    }

    function deleteVendor($id){
        //set conditional
        $where = array('vendorId' => $id);

        //check vendorId
        $chek = $this->vendor_model->detailVendor($where,'vendor')->num_rows();

        if($chek>0){
            $this->vendor_model->doDeleteVendor($where, 'vendor');

            $message = "Data Vendor berhasil dihapus";
            $this->session->set_flashdata('message', $message);
            redirect('vendor/index');
        }
        else{
            $message = "ID vendor tidak ditemukan";
            $this->session->set_flashdata('message', $message);
            redirect('vendor/index');
        }
    }
}


