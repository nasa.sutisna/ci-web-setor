<?php

function rupiah($value){
 return "Rp " . number_format($value, 2, ',', '.');
}

function dateFormat($date){
	return date("d/m/Y", strtotime($date));
}

function middlewareAdmin(){
	$role = $_SESSION['role'];
	if($role == 1){
		return true;
	}
	else{
		redirect('home');
	}
}

function isLogged(){
	$role = $_SESSION['fullName'];
	if($role){
		return true;
	}
	else{
		return false;
	}
}

function checkAuth(){
	$session = isLogged();
	if($session == false){
		redirect('member/login');
	}
}
